//
//  ModelSupport.swift
//  PacificPalmsProperty
//
//  Created by mac on 06/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

typealias JsonDictionay = [String : Any]


protocol CreateFromArray {
    
    init?(json: JsonDictionay )
}

extension CreateFromArray {
    
    ///Create array of items from json
    static func createRequiredInstances(from json: JsonDictionay , key:String) -> [Self]? {
        guard let jsonDictionaries = json[key] as? [[String: Any]] else { return nil }
        var array = [Self]()
        for jsonDictionary in jsonDictionaries {
            guard let instance = Self.init(json: jsonDictionary) else { return nil }
            array.append(instance)
        }
        return array
    }
}
