//
//  Message.swift
//  PacificPalmsProperty
//
//  Created by mac on 06/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class  messages {
    static  var email_empty : String {
        return"Please enter your Email ID"
    }
    static var name_empty: String {
        return"Please enter your name"
    }
    
    static  var valid_email : String {
        return"Please enter valid email ID"
    }
    
    static  var password_empty : String {
        return "Please enter your password"
    }
    
    static  var registered_email_empty : String {
        return "Please enter your registered Email ID"
    }
    static  var confirmP_empty : String {
        return "Please enter confirm password"
    }
    static  var password_not_match: String {
        return"Password does not matched"
    }
    static  var New_password_not_match: String {
        return"New password and confirm password does not match."
    }
    static  var confirm_password_valid: String {
        return"Confirm password should be greater than 8 characters"
    }
    
    static  var password_valid: String {
        return"The password must be at least 8 characters"
    }
    
    static  var OTP_empty: String {
        return"Please enter OTP."
    }
    

}
