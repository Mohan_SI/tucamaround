//
//  TabbarVC.swift
//  PacificPalmsProperty
//
//  Created by mac on 07/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController, UITabBarControllerDelegate {
    
    static func viewController () -> TabbarVC {
        return "Home".viewController("TabbarVC") as! TabbarVC
    }
    
    @IBOutlet var viewPopup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
        self.delegate = self
    }
    
    //MARK: ACTIONS
    @IBAction func actionRegister(_ sender: Any) {
        if let url = URL(string: "https://www.google.com/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        kAppDelegate.isGuest = ""
        viewPopup.removeFromSuperview()
        let vc = SignInVC.viewController()
        vc.push(self)
    }
    
    @IBAction func closePopup(_ sender: Any) {
        viewPopup.removeFromSuperview()
    }
        
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if kAppDelegate.isGuest == "1" {
            if tabBarController.selectedIndex != 0 {
                addPopup()
                tabBarController.selectedIndex = 0
            }
        }
    }
    
    //MARK: FUNCTIONS
    func addPopup() {
        viewPopup.frame = kAppDelegate.window!.frame
        kAppDelegate.window!.addSubview(viewPopup)
    }
}
