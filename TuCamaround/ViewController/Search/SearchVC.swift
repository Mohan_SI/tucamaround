//
//  SearchVC.swift
//  PacificPalmsProperty
//
//  Created by mac on 20/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlacePicker

class SearchVC: UIViewController {
    
    static func viewController () -> SearchVC {
        return "Home".viewController("SearchVC") as! SearchVC
    }
 
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tfSearch: UITextField!

    var arrSelected:[Int] = []
    var arrList:[ProjectObject] = []
    var showShimmer = true
    var dataPostList = [PostListData]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if kAppDelegate.isGuest != "1" {
            DispatchQueue.main.async {
                self.showShimmer = true
                self.tblView.reloadData()
                self.tblView.showLoader()
            }
            
            if tfSearch.text?.count != 0 {
                tfSearch.text = ""
            }

            wsPostList(lat: kAppDelegate.locationManager.location?.coordinate.latitude ?? 0.0, long: kAppDelegate.locationManager.location?.coordinate.longitude ?? 0.0, location: kAppDelegate.currentAddress)
        }
    }
    
    //MARK: ACTIONS
    @IBAction func actionSearch(_ sender: UIButton) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
 
    //MARK: wsPostList
    func wsPostList (lat: Double, long: Double, location:String) {
        dataPostList = [PostListData]()
        
        self.view.endEditing(true)

        let param = params()
        
        param["latitude"] = lat
        param["longitude"] = long
        param["location"] = location

        let httpPara = HttpParams (api_post_list)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = false

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            self.showShimmer = false
            self.tblView.hideLoader()

            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(PostListJSON.self, from: data)
                            if let data = jsonn.data {
                                if data.count == 0 {
                                    //self.setHideVw()
                                } else {
                                    self.dataPostList = data
                                    self.tblView.reloadData()
                                }
                            }
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    self.dataPostList = [PostListData]()
                    self.tblView.reloadData()
                }
            } else {
                self.tblView.reloadData()
            }
        }
    }
}

extension SearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showShimmer {
            return 15
        }
        return dataPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchViewCell") as!  SearchViewCell
        if !showShimmer {
            let ob = dataPostList[indexPath.row]
            cell.lblTitle.text = ob.serviceName?.capFirstLetter()
            cell.lblName.isHidden = true
            cell.lblPrice.isHidden = true

            if let bidData = ob.bidData {
                if bidData.jobAmount != nil {
                    cell.lblName.isHidden = false
                    cell.lblPrice.isHidden = false
                    
                    if bidData.jobAmount != "" {
                        cell.lblPrice.text = Int(bidData.jobAmount ?? "")!.withCommas()
                    }
                }
            }
            
            if let distance = ob.distance {
                if distance != "" {
                    let dis = Double(distance)
                    cell.lblSubTitle.text = String(format: "%.2f km   |  \(ob.bidCount ?? "") Cotización", dis ?? 0.0)
                } else {
                    cell.lblSubTitle.text = "\(ob.bidCount ?? "") Cotización"
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ob = dataPostList[indexPath.row]
        
        let dictJob = NSMutableDictionary()
        dictJob["name"] = ob.serviceName?.capFirstLetter() ?? ""
        dictJob["id"] = ob.id ?? ""
        dictJob["user_id"] = ob.userId ?? ""
        
        let vc = ProjectDetailVC.viewController()
        vc.dictJob = dictJob
        vc.push(self)
    }
    
    @objc func actionRepeat(btn:UIButton) {
    }
}

extension SearchVC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        DispatchQueue.main.async {
            self.showShimmer = true
            self.tblView.reloadData()
            self.tblView.showLoader()
        }
        self.wsPostList(lat: place.coordinate.latitude, long: place.coordinate.longitude, location: place.formattedAddress ?? "")
        self.tfSearch.text = place.formattedAddress
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

struct PostListJSON: Codable {
    let status: String?
    let data  : [PostListData]?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case status, data, message
    }
}

class PostListData: Codable {

    let address: String?
    let bookingAmount: String?
    let bookingStatus: String?
    let categoryId: String?
    let country: String?
    let createdBy: String?
    let createdOn: String?
    let id: String?
    let isRated: String?
    let latitude: String?
    let longitude: String?
    let paymentStatus: String?
    let paymentType: String?
    let postNumber: String?
    let serviceDetails: String?
    let serviceName: String?
    let status: String?
    let subcategoryId: String?
    let techId: String?
    let totalAmount: String?
    let updatedBy: String?
    let updatedOn: String?
    let userId: String?
    let bidData:PostBidData!
    let bidCount: String?
    let distance: String?

    enum CodingKeys: String, CodingKey {
        case address, country, id, latitude, longitude, status, distance
        case bookingAmount = "booking_amount"
        case bookingStatus = "booking_status"
        case categoryId = "category_id"
        case createdBy = "created_by"
        case createdOn = "created_on"
        case isRated = "is_rated"
        case paymentStatus = "payment_status"
        case paymentType = "payment_type"
        case postNumber = "post_number"
        case serviceDetails = "service_details"
        case serviceName = "service_name"
        case subcategoryId = "subcategory_id"
        case techId = "tech_id"
        case totalAmount = "total_amount"
        case updatedBy = "updated_by"
        case updatedOn = "updated_on"
        case userId = "user_id"
        case bidData = "bid_data"
        case bidCount = "bid_count"
    }
}
