//
//  SearchViewCell.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SearchViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellTitle(ob:ProjectObject){
        
        lblTitle.text = ob.title
        lblSubTitle.text = ob.subTitle
        
    }
}
