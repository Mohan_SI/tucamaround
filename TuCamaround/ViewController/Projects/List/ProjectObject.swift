//
//  Item.swift
//  MVVM-Movies
//
//  Created by Mario Acero on 1/16/18.
//  Copyright © 2018 Mario Acero. All rights reserved.
//

import Foundation


/// Model for objet Movie or Tv Show containes all info to show in list or details
struct ProjectObject: CreateFromArray{
    
    /// Id from db
    let id              : Int
    /// Principal title of the Movie
    let title           : String
    /// Average from votes
    
    let subTitle    : String
    /// Synopsis of the movie or Tv Show
   let status        : OrderStatus
    

    
    ///Receive parameters for initializer the struct from api, is called from init JSON
    init(id:Int,title:String,subTitle:String , status:OrderStatus){
        self.id            = id
        self.title         = title
        self.subTitle  = subTitle
        
        self.status      = status
    }
    
    /**
     This init receive and validate data from Json, return nil in case the struct not is available, if all data is true  call super init fof the struct
     - Parameter json : Data from Api Rest
     */
    init?(json: JsonDictionay) {
 
        
        
        guard let id            = json["id"] as? Int else { return nil }
        let title               = json["title"] as? String  ?? ""
        let name                = json["subTitle"] as? String  ?? ""
        let status = getStaus(status: json["status"] as? String  ?? "")
        self.init(id:id,title:title,subTitle:name, status:status)
   //     self.init()
    }
}

func getStaus(status : String) ->OrderStatus {
    let status = status.lowercased()
    if status == "awarded" {
        return .awarded
    }else if status == "in progress" {
        return .inProgress
    }else if status == "completed" {
        return .complete
    }else if status == "cancelled" {
        return .cancelled
    }else if status == "in review" {
        return .inReview
    }
    return .pending
}
