//
//  ProjectsVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}

class ProjectsVC: UIViewController {
    
    static func viewController () -> ProjectsVC {
        return "Home".viewController("ProjectsVC") as! ProjectsVC
    }
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewDotBorder: UIView!

    @IBOutlet var viewPopupFilter: UIView!
    @IBOutlet var tblStatus: UITableView!
    @IBOutlet var consHtTbl: NSLayoutConstraint!
    
    var showShimmer = true
    var seleIndex = 0
    var dataJobList = [JobListData]()
    var arr = [JobListData]()
    var boolShouldDownload = true
    var pageCount:Int = 1
    var pointContentOffset = CGPoint.zero
    
    var arrStatus: [[String: String]] = [
    ["name": "Todos", "status": "all"],
    ["name": "En Progreso", "status": "inprogress"],
    ["name": "Completados", "status": "completed"],
    ["name": "Abiertos", "status": "awarded"],
    ["name": "Cerrados", "status": "cancelled"]]
       
    override func viewDidLoad() {
        super.viewDidLoad()
        consHtTbl.constant = CGFloat(arrStatus.count * 44)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewDotBorder.dashedBorder(.hexColor(0xBCBFC6))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if kAppDelegate.isGuest == "1" {
            tblView.isHidden = true
        } else {
            tblView.isHidden = false
            callApi()
        }
    }
    
    //MARK: ACTIONS
    @IBAction func actionFilter(_ sender: Any) {
        addPopup()
    }
    
    @IBAction func actionClose(_ sender: Any) {
        viewPopupFilter.removeFromSuperview()
    }
    
    @IBAction func actionApply(_ sender: Any) {
        viewPopupFilter.removeFromSuperview()
        callApi()
    }
    
    //MARK: FUNCTIONS
    func setHideVw() {
        boolShouldDownload = false
        dataJobList = [JobListData]()
        tblView.reloadData()
        tblView.isHidden = true
        viewDotBorder.isHidden = false
    }
    
    func callApi() {
        tblView.isHidden = false
        viewDotBorder.isHidden = true
        dataJobList = [JobListData]()
        DispatchQueue.main.async {
            self.showShimmer = true
            self.tblView.reloadData()
            self.tblView.showLoader()
        }
        pageCount = 1
        boolShouldDownload = true
        let dt = arrStatus[seleIndex]
        wsPostList(action: dt["status"]!)
    }
    
    func addPopup() {
        viewPopupFilter.frame = self.view.frame
        self.view.addSubview(viewPopupFilter)
    }
    
    //MARK: wsPostList
    func wsPostList (action:String) {
        pointContentOffset = tblView.contentOffset

        self.view.endEditing(true)

        let param = params()
        param["action"] = action
        param["page"] = pageCount

        let httpPara = HttpParams (api_my_job_list)
        httpPara.params = param
        httpPara.popup = true

        if pageCount == 1 {
            httpPara.aai = false
        } else {
            httpPara.aai = true
        }
        
        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            self.showShimmer = false
            self.tblView.hideLoader()

            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(JobListJSON.self, from: data)
                            if let data = jsonn.data {
                                if data.count == 0 {
                                    if self.pageCount == 1 {
                                        self.setHideVw()
                                    }
                                } else {
                                    if self.pageCount == 1 {
                                        self.arr.removeAll()
                                        self.arr = data
                                    } else {
                                        for i in 0..<data.count {
                                            self.arr.append(data[i])
                                        }
                                    }
                                    
                                    self.dataJobList = self.arr
                                    self.boolShouldDownload = false
                                    self.tblView.reloadData()
                                }
                            }
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    //Http.alert("", string(json!, "message"))
                    if self.pageCount == 1 {
                        self.setHideVw()
                    }
                }
            } else {
                self.tblView.reloadData()
            }
        }
    }
}

extension ProjectsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            if showShimmer {
                return 15
            }
            return dataJobList.count
        } else {
            return arrStatus.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectsViewCell") as!  ProjectsViewCell
            
            if !showShimmer {
                let ob = dataJobList[indexPath.row]
                cell.lblTitle.text = ob.serviceName?.capFirstLetter()
                
                cell.lblSubTitle.text = ""

                if let bidData = ob.bidData {
                    if bidData.jobAmount != nil {
                        if bidData.jobAmount != "" {
                            cell.lblSubTitle.text = "\(bidData.firstName?.capFirstLetter() ?? "") -  \(Int(bidData.jobAmount ?? "")!.withCommas()) USD en \(bidData.workingDays ?? "") días"
                        }
                    }
                }
                
                let date = ob.createdOn?.getTDate()
                if date != nil {
                    cell.lblDate.text = "\(date!.getStringDate("dd")) \(date!.getStringDate("MMM").getDtt())"
                }
                
                cell.btnVew.setButtonColor(title: ob.bookingStatus?.lowercased() ?? "")
                
                if indexPath.row == (dataJobList.count - 1) && (tblView.contentOffset.y > pointContentOffset.y) {
                    if !boolShouldDownload {
                        boolShouldDownload = true
                        if dataJobList.count % 10 == 0 {
                            pageCount += 1
                            boolShouldDownload = true
                            
                            let dt = arrStatus[seleIndex]
                            wsPostList(action: dt["status"]!)
                        }
                    }
                }
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
            
            let dt = arrStatus[indexPath.row]
            cell.lblName.text =  dt["name"]
            
            if seleIndex == indexPath.row {
                cell.imgRadio.image = UIImage(named: "orange_circle")
                cell.lblName.textColor = #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1)
            } else {
                cell.imgRadio.image = UIImage(named: "radio_uncheck")
                cell.lblName.textColor = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
            }

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView {
            let ob = dataJobList[indexPath.row]
            
            let dictJob = NSMutableDictionary()
            dictJob["name"] = ob.serviceName?.capFirstLetter() ?? ""
            dictJob["id"] = ob.id ?? ""
            dictJob["user_id"] = ob.userId ?? ""
            
            let vc = ProjectDetailVC.viewController()
            vc.dictJob = dictJob
            vc.push(self)
        } else {
            seleIndex = indexPath.row
            tableView.reloadData()
        }
    }
    
    @objc func actionRepeat(btn:UIButton) {
        tblView.reloadData()
    }
}

struct JobListJSON: Codable {
    let status: String?
    let data  : [JobListData]?
    let message : String?

    enum CodingKeys: String, CodingKey {
        case status, data, message
    }
}

class JobListData: Codable {

    let address: String?
    let bookingAmount: String?
    let bookingStatus: String?
    let categoryId: String?
    let country: String?
    let createdBy: String?
    let createdOn: String?
    let id: String?
    let isRated: String?
    let latitude: String?
    let longitude: String?
    let paymentStatus: String?
    let paymentType: String?
    let postNumber: String?
    let serviceDetails: String?
    let status: String?
    let subcategoryId: String?
    let techId: String?
    let totalAmount: String?
    let updatedBy: String?
    let updatedOn: String?
    let userId: String?
    let bidData: PostBidData?
    let serviceName:String?

    enum CodingKeys: String, CodingKey {
        case address, country, id, latitude, longitude, status
        case bookingAmount = "booking_amount"
        case bookingStatus = "booking_status"
        case categoryId = "category_id"
        case createdBy = "created_by"
        case createdOn = "created_on"
        case isRated = "is_rated"
        case paymentStatus = "payment_status"
        case paymentType = "payment_type"
        case postNumber = "post_number"
        case serviceDetails = "service_details"
        case subcategoryId = "subcategory_id"
        case techId = "tech_id"
        case totalAmount = "total_amount"
        case updatedBy = "updated_by"
        case updatedOn = "updated_on"
        case userId = "user_id"
        case bidData = "bid_data"
        case serviceName = "service_name"
    }
}
