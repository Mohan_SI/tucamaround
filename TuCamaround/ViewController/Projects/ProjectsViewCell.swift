//
//  ProjectsViewCell.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ProjectsViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnVew: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBg: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCellTitle(ob:ProjectObject){
        lblTitle.text = ob.title
        lblSubTitle.text = ob.subTitle
        lblDate.text = "20 Ene"
        
        if ob.status  == .awarded {
            btnVew.setTitle("Adjudicado", for: .normal)
            btnVew.backgroundColor = #colorLiteral(red: 0.985673964, green: 0.8888066411, blue: 0.8899743557, alpha: 1)
            btnVew.setTitleColor(#colorLiteral(red: 1, green: 0.3529411765, blue: 0.3725490196, alpha: 1), for: .normal)
        }  else if ob.status  == .inProgress {
            btnVew.setTitle("En Progreso", for: .normal)
            btnVew.backgroundColor = #colorLiteral(red: 0.990672648, green: 0.9522123933, blue: 0.8773685098, alpha: 1)
            btnVew.setTitleColor(#colorLiteral(red: 1, green: 0.6980392157, blue: 0.337254902, alpha: 1), for: .normal)
        }else if ob.status  == .complete {
            btnVew.setTitle("Completado", for: .normal)
            btnVew.backgroundColor = appColor.theamGreenColor.withAlphaComponent(0.2)
            btnVew.setTitleColor(appColor.theamGreenColor, for: .normal)
        }else if ob.status  == .inReview {
            btnVew.setTitle("En revisión", for: .normal)
            btnVew.backgroundColor = appColor.error.withAlphaComponent(0.2)
            btnVew.setTitleColor(appColor.error, for: .normal)
        }else if ob.status  == .cancelled {
            btnVew.setTitle("Cancelado", for: .normal)
            btnVew.backgroundColor = appColor.border
            btnVew.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1), for: .normal)
        }
    }
}
