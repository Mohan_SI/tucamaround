//
//  RadiusCell.swift
//  TuCamaround
//
//  Created by mac on 25/04/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol RadiusDelegate {
    func actionSelectAddress()
    func actionSubmitAddress(address: String, radius: String)
}

class RadiusCell: UITableViewCell {

    @IBOutlet var tfAddress: UITextField!
    @IBOutlet var tfRadius: UITextField!
    @IBOutlet var btnSelectAddress: UIButton!

    var delegate:RadiusDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func actionSelectAddress(_ sender: Any) {
        delegate.actionSelectAddress()
    }
    
    @IBAction func actionSubmitAddress(_ sender: Any) {
        delegate.actionSubmitAddress(address: tfAddress.text!, radius: tfRadius.text!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
