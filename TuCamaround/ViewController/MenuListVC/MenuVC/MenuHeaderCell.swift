//
//  MenuHeaderCell.swift
//  TuCamaround
//
//  Created by mac on 23/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MenuHeaderCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMyBalance: UILabel!
    @IBOutlet var lblPending: UILabel!
    @IBOutlet var lblMyCamaround: UILabel!
    @IBOutlet var lblAvg: UILabel!
    @IBOutlet var lblIncome: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
