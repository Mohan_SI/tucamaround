//
//  ExploreVC.swift
//  PacificPalmsProperty
//
//  Created by mac on 12/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import GooglePlacePicker

class MenuVC: UIViewController {
    
    static func viewController () -> MenuVC {
        return "Home".viewController("MenuVC") as! MenuVC
    }
    
    @IBOutlet weak var tblView: UITableView!
 
    var dataDashboard:DashboardData?
    var lat:Double!
    var long:Double!
    var address = ""
    var dataInprogress = [InprogressListData]()
    var dataAwarded = [InprogressListData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if kAppDelegate.isGuest != "1" {
            DispatchQueue.main.async {
                self.view.showLoader()
            }
            wsDashboard()
        }
    }

    //MARK: wsDashboard
    func wsDashboard () {
        self.view.endEditing(true)

        let param = params()

        let httpPara = HttpParams (api_tech_dashboard)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = false

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            self.view.hideLoader()

            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(DashboardJSON.self, from: data)
                            if let data = jsonn.data {
                                self.dataDashboard = data
                            }
                            
                            if let inprogressList = jsonn.inprogressList {
                                self.dataInprogress = inprogressList
                            }
                            
                            if let awardedList = jsonn.awardedList {
                                self.dataAwarded = awardedList
                            }
                            
                            self.tblView.reloadData()

                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsTectAddress
    func wsTectAddress (location:String, radius: String) {
        self.view.endEditing(true)

        let param = params()
        param["latitude"] = lat
        param["longitude"] = long
        param["location"] = location
        param["radious"] = radius

        let httpPara = HttpParams (api_tech_address)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            self.view.hideLoader()

            if json != nil {
                if json!.success() {
                    self.wsGetProfile()
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsGetProfile
    func wsGetProfile () {
        self.view.endEditing(true)
        
        let param = params()

        let httpPara = HttpParams (api_get_my_profile)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    let ob = json?.data()
                    if ob?.data != nil {
                        ob?.ss?.saveUserInfo()
                    }
                    if let images = json?.object(forKey: "images") as? NSArray {
                        kAppDelegate.storeImages(images.mutableCopy() as! NSMutableArray)
                    }
                    self.tblView.reloadData()
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension MenuVC: UITableViewDelegate, UITableViewDataSource, RadiusDelegate, HomeTableDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if kAppDelegate.isGuest == "1" {
            return 3
        } else {
            let ob = LoginJSON.userInfo()
            if ob?.data?.address != "" {
                return 3
            } else {
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if kAppDelegate.isGuest == "1" {
            if section == 0 {
                return 1
            }else  if section == 1 {
                return 0
            } else {
                return 0
            }
        } else {
            let ob = LoginJSON.userInfo()
            if ob?.data?.address != "" {
                if section == 0 {
                    return 1
                }else  if section == 1 {
                    return dataAwarded.count
                } else {
                    return dataInprogress.count
                }
            } else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section  == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeaderCell") as!  MenuHeaderCell
            
            if kAppDelegate.isGuest == "1" {
                cell.lblName.text = "Bienvenido"
            } else {
                let ob = LoginJSON.userInfo()
                if let name = ob?.data?.firstName {
                    cell.lblName.text = "Hola " + name.capFirstLetter()
                }
                
                if dataDashboard != nil {
                    if let myBalace = dataDashboard?.mybalance {
                        if myBalace != "" {
                            cell.lblMyBalance.text = "\(Int(myBalace)!.withCommas())"
                            cell.lblIncome.text = "\(Int(myBalace)!.withCommas())"
                        }
                    }
                    
                    if let pendingAmt = dataDashboard?.pendingAmt {
                        if pendingAmt != "" {
                            cell.lblPending.text = "\(Int(pendingAmt)!.withCommas())"
                        }
                    }
                    
                    cell.lblMyCamaround.text = dataDashboard?.countsJobs ?? "0"
                    cell.lblAvg.text = dataDashboard?.pendingJobs ?? "0"
                }
            }
            
            return cell

        }else {
            let obLogin = LoginJSON.userInfo()
            if obLogin?.data?.address != "" {
                if indexPath.section == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as!  HomeTableCell
                    
                    let ob = dataAwarded[indexPath.row]
                    
                    cell.lblTitle.text = ob.bidData?.capFirstLetter()
                    cell.lblSubTitle.text = ""
                    
                    if ob.jobAmount != nil {
                        if ob.jobAmount != "" {
                            cell.lblSubTitle.text = "\(ob.firstName?.capFirstLetter() ?? "") -  \(Int(ob.jobAmount ?? "")!.withCommas()) USD en \(ob.workingDays ?? "") días"
                        }
                    }
                    
                    cell.btnSeeMore.isHidden = true
                    if indexPath.row == 0 {
                        cell.lblTitleType.text = "CAMAROUNDS ADJUDICADOS".uppercased()
                        cell.lblTitleType.isHidden = false
                    }else {
                        cell.lblTitleType.isHidden = true
                    }
                    
                    cell.btnVew.setTitle("Ver", for: .normal)
                    cell.btnVew.setTitleColor(UIColor.white, for: .normal)
                    cell.btnVew.backgroundColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0, alpha: 1)
                    
                    cell.delegate = self
                    
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as!  HomeTableCell
                    
                    let ob = dataInprogress[indexPath.row]
                    
                    cell.lblTitle.text = ob.bidData?.capFirstLetter()
                    cell.lblSubTitle.text = ""
                    
                    if ob.jobAmount != nil {
                        if ob.jobAmount != "" {
                            cell.lblSubTitle.text = "\(ob.firstName?.capFirstLetter() ?? "") -  \(Int(ob.jobAmount ?? "")!.withCommas()) USD en \(ob.workingDays ?? "") días"
                        }
                    }
                    
                    if indexPath.row == 0 {
                        cell.btnSeeMore.isHidden = false
                        cell.lblTitleType.text = "Últimos proyectos".uppercased()
                        cell.lblTitleType.isHidden = false
                    }else {
                        cell.lblTitleType.isHidden = true
                        cell.btnSeeMore.isHidden = true
                    }
                    
                    //cell.btnVew.setTitle("En progreso", for: .normal)
                    //cell.btnVew.setTitleColor(#colorLiteral(red: 0.2549019608, green: 0.6156862745, blue: 0.7843137255, alpha: 1), for: .normal)
                    //cell.btnVew.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.6156862745, blue: 0.7843137255, alpha: 1).withAlphaComponent(0.2)
                    
                    cell.btnVew.setTitle("En curso", for: .normal)
                    cell.btnVew.setTitleColor(UIColor.white, for: .normal)
                    cell.btnVew.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.4549019608, blue: 0, alpha: 1)
                    
                    cell.delegate = self
                    
                    return cell
                }
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RadiusCell") as!  RadiusCell
                cell.tfAddress.superview?.border(.hexColor(0xE0E2E6), 8, 1)
                cell.tfRadius.superview?.border(.hexColor(0xE0E2E6), 8, 1)
                cell.tfAddress.text = address

                cell.delegate = self
                
                return cell
            }
        }
    }
    
    func actionSeeMore() {
        self.tabBarController?.selectedIndex = 1
        let vc = MenuVC.viewController()
        vc.push(self)
    }
    
    func actionSelectAddress() {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    func actionSubmitAddress(address: String, radius: String) {
        self.view.endEditing(true)
        if address.count == 0 {
            Toast.toast(ValidationClass.select_address)
        } else if radius.count == 0 {
            Toast.toast(ValidationClass.blank_radius)
        } else {
            wsTectAddress(location: address, radius: radius)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obl = LoginJSON.userInfo()
        if obl?.data?.address != "" {
            var dictJob = NSMutableDictionary()

            if indexPath.section == 1 {
                let ob = dataAwarded[indexPath.row]
                
                dictJob = NSMutableDictionary()
                dictJob["name"] = ob.bidData?.capFirstLetter() ?? ""
                dictJob["id"] = ob.jobId ?? ""
                dictJob["user_id"] = ob.userId ?? ""
                
                let vc = ProjectDetailVC.viewController()
                vc.dictJob = dictJob
                vc.push(self)
                
            } else if indexPath.section == 2 {
                let ob = dataInprogress[indexPath.row]
                
                dictJob["name"] = ob.bidData?.capFirstLetter() ?? ""
                dictJob["id"] = ob.jobId ?? ""
                dictJob["user_id"] = ob.userId ?? ""
                
                
                let vc = ProjectDetailVC.viewController()
                vc.dictJob = dictJob
                vc.push(self)
            }
        }
    }
}

extension MenuVC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        lat = place.coordinate.latitude
        long = place.coordinate.longitude
        address = place.formattedAddress ?? ""
        
        tblView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

struct DashboardJSON: Codable {
    let status: String?
    let data  : DashboardData?
    let message : String?
    let inprogressList:[InprogressListData]?
    let awardedList:[InprogressListData]?

    enum CodingKeys: String, CodingKey {
        case status, data, message
        case inprogressList = "inprogress_list"
        case awardedList = "awarded_list"
    }
}

class DashboardData: Codable {

    let mybalance: String?
    let countsJobs: String?
    let pendingAmt: String?
    let pendingJobs: String?

    enum CodingKeys: String, CodingKey {
        case mybalance
        case countsJobs = "counts_jobs"
        case pendingAmt = "pending_amt"
        case pendingJobs = "pending_jobs"
    }
}


class InprogressListData: Codable {

    let adminCommision: String?
    let adminPercent: String?
    let bidData: String?
    let bidStatus: String?
    let comment: String?
    let createdOn: String?
    let firstName: String?
    let id: String?
    let jobAmount: String?
    let jobId: String?
    let lastName: String?
    let materialInclude: String?
    let paymentType: String?
    let performance: String?
    let profileImage: String?
    let rating: String?
    let techId: String?
    let totalReviews: String?
    let updatedOn: String?
    let userId: String?
    let workingDays: String?

    enum CodingKeys: String, CodingKey {
        case comment, id, performance, rating
        case adminCommision = "admin_commision"
        case adminPercent = "admin_percent"
        case bidData = "bid_data"
        case bidStatus = "bid_status"
        case createdOn = "created_on"
        case firstName = "first_name"
        case jobAmount = "job_amount"
        case jobId = "job_id"
        case lastName = "last_name"
        case materialInclude = "material_include"
        case paymentType = "payment_type"
        case profileImage = "profile_image"
        case techId = "tech_id"
        case totalReviews = "total_reviews"
        case updatedOn = "updated_on"
        case userId = "user_id"
        case workingDays = "working_days"
    }
}

extension UIButton {
    func setButtonColor (title: String) {
        self.setTitleColor(UIColor.white, for: .normal)

        if title == "awarded" {
            //self.setTitleColor(#colorLiteral(red: 1, green: 0.3529411765, blue: 0.3725490196, alpha: 1), for: .normal)
            //self.backgroundColor = #colorLiteral(red: 1, green: 0.3529411765, blue: 0.3725490196, alpha: 1)
            self.backgroundColor = #colorLiteral(red: 0.03529411765, green: 0.3647058824, blue: 0.6509803922, alpha: 1)
            self.setTitle("Adjudicada", for: .normal)

        } else if title == "completed" {
            //self.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.8862745098, blue: 0.7803921569, alpha: 1), for: .normal)
            //self.backgroundColor = #colorLiteral(red: 0.0862745098, green: 0.8862745098, blue: 0.7803921569, alpha: 1)
            
            self.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.8941176471, blue: 0.8196078431, alpha: 1)
            self.setTitle("Completada", for: .normal)

        } else if title == "cancelled" {
            //self.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1), for: .normal)
            //self.backgroundColor = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
            
            self.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            self.setTitle("Cancelada", for: .normal)

        } else if title == "inprogress" {
            //self.setTitleColor(#colorLiteral(red: 0.6823529412, green: 0, blue: 0, alpha: 1), for: .normal)
            //self.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0, blue: 0, alpha: 1)
            self.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.4549019608, blue: 0, alpha: 1)
            self.setTitle("En curso", for: .normal)
            
        } else if title == "inreview" {
            self.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.1098039216, blue: 0, alpha: 1)
            self.setTitle("En revisión", for: .normal)
            
        } else { //Open
            //self.setTitleColor(#colorLiteral(red: 1, green: 0.6980392157, blue: 0.337254902, alpha: 1), for: .normal)
            //self.backgroundColor = #colorLiteral(red: 1, green: 0.6980392157, blue: 0.337254902, alpha: 1)
            
            self.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.6745098039, blue: 0.168627451, alpha: 1)
            self.setTitle("Disponible", for: .normal)
        }
//        let color = self.titleLabel?.textColor
//        self.backgroundColor = color?.withAlphaComponent(0.3)
    }
}
