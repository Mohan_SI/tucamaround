//
//  ShimmerHomeView.swift
//  DsirUser
//
//  Created by mac on 10/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ShimmerHomeView: UIView {
    
    static let instance = ShimmerHomeView.initLoader()
    
    class func initLoader() -> ShimmerHomeView {
        //        instance.origin = yAXIS
        return ShimmerHomeView()// UINib(nibName: "OrderShimmer", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderShimmer
    }
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.white
    }
    var yAxs:CGFloat = 0.0
    func showShimmer()->UIView{
        
        addShimmmerView()
        return self
    }
    func addShimmmerView(){
        let mainWidth = UIScreen.main.bounds.width
        var viewOrigin:CGFloat = 0.0
        let rect = CGRect(x: 0, y: viewOrigin, width: mainWidth, height: UIScreen.main.bounds.height)
        
        let viewMainBG = UIView(frame: CGRect(x: 0, y: 0, width: mainWidth, height: UIScreen.main.bounds.height))
        let shimmerView = UIView(frame: rect)

        
        let  viewShimmer = ShimmeringView(frame: CGRect(x: 0, y: 0, width: mainWidth, height: UIScreen.main.bounds.height))
        
        
        viewMainBG.addSubview(viewShimmer)
        

        shimmerView.backgroundColor = .white
        viewOrigin = viewOrigin + 20
        for ii in 0 ..< 10 {
            
            let viewMain2 = UIView(frame: CGRect(x: 16, y: CGFloat(ii) * 200 + viewOrigin, width: mainWidth - 32, height: 200))
            let vieww0 = UIView(frame: CGRect(x: 0, y: 10, width: mainWidth - 32, height: 180))
   
            
            vieww0.layer.cornerRadius = 4
            viewMain2.addSubview(vieww0)
            shimmerView.addSubview(viewMain2)
            
            vieww0.backgroundColor = appColor.shimmerColor

        }
   
        self.addSubview(viewMainBG)
        viewMainBG.backgroundColor = UIColor.white
        viewShimmer.isShimmering = true
        viewShimmer.contentView = shimmerView
        
    }
}
