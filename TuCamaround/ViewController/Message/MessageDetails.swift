//
//  MessageDetails.swift
//  TuCamarounds
//
//  Created by mac on 27/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MessageDetails: UIViewController {

    static func viewController () -> MessageDetails {
        return "Messages".viewController("MessageDetails") as! MessageDetails
    }
    
    @IBOutlet var tfMessage: UITextField!
    @IBOutlet var btnPaintWall: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        btnPaintWall.setAttributedTitle(getAtr(txt: "Paint a Wall", clr: #colorLiteral(red: 0.1004235372, green: 0.1446691453, blue: 0.2420555651, alpha: 1)), for: .normal)
        
        tfMessage.superview?.border(.hexColor(0xB9BCCD), 8, 1)
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    //MARK: FUNCTIONS
    func getAtr(txt: String, clr:UIColor) -> NSMutableAttributedString {
        let fontStyle = UIFont(name: "Rubik-Regular", size: 18.0)!
        
        let atr = NSMutableAttributedString(string: txt, attributes: [NSAttributedString.Key.foregroundColor: clr,NSAttributedString.Key.font : fontStyle, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        let final = NSMutableAttributedString()
        final.append(atr)
        
        return final
    }
}
 
extension MessageDetails: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
