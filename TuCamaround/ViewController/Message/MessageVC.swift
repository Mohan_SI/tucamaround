//
//  MessageVC.swift
//  TuCamaround
//
//  Created by mac on 01/04/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MessageVC: UIViewController {
    static func viewController () -> MessageVC {
        return "Home".viewController("MessageVC") as! MessageVC
    }
    
    let viewShimmer = ShimmerHomeView.instance.showShimmer()
    var issimmer = true
    @IBOutlet weak var tblView: UITableView!
    var refreshControl = UIRefreshControl()
 
    var arrList:[ProjectObject] = []
    
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var btnSearch: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.estimatedRowHeight = 200.0
        self.tblView.rowHeight = UITableView.automaticDimension
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)
        
        let deadline = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.viewShimmer.removeFromSuperview()
            self.issimmer = false
            self.tblView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if kAppDelegate.isGuest == "1" {
            tblView.isHidden = true
        } else {
            tblView.isHidden = false
        }
    }
    @objc func refresh(sender:AnyObject) {
        
        //        apiForOrderList()
        self.issimmer = true
        
        
        self.tblView.reloadData()
        let deadline = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refreshControl.endRefreshing()
            self.issimmer = false
            DispatchQueue.main.async {
                self.viewShimmer.removeFromSuperview()
                
                self.tblView.reloadData()
            }
        }
    }
    
    //MARK: ACTIONS
    @IBAction func actionSearch(_ sender: Any) {
        viewSearch.isHidden = false
        btnSearch.isHidden = true
    }
    
    @IBAction func actionSearchUnder(_ sender: Any) {
        viewSearch.isHidden = true
        btnSearch.isHidden = false
    }
}


extension MessageVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if issimmer {
            tblView.addSubview(self.viewShimmer)
            return 0
        }
        
        return 10//arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as!  MessageCell
 
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        MessageDetails.viewController().push(self)
    }
    
 
}

extension MessageVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
