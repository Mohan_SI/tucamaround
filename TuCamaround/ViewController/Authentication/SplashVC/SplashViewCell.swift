//
//  SplashViewCell.swift
//  Borobear
//
//  Created by mac on 24/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class SplashViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
