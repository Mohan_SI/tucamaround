//
//  SplashVC.swift
//  Borobear
//
//  Created by mac on 24/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    static func viewController () -> SplashVC {
        return "Authentication".viewController("SplashVC") as! SplashVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
  //       AppActivity.setRemoveActivitys()
     }
    
    override func viewDidAppear(_ animated: Bool) {
        statusBarColorChange(color: #colorLiteral(red: 0.9960784314, green: 0.2352941176, blue: 0, alpha: 1))
        self.view.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.2352941176, blue: 0, alpha: 1)
        
        generateAccessToken(completionHandler: { (token) in
        })
        
            let seconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//                if !AppActivity.isShowSplash {
//                    SplashNextVC.viewController().push(self)
//                }else if AppActivity.isUserLogin {
//                    let vc = TabbarVC.viewController()
//                      vc.push(self)
//                } else {
//                    SignInVC.viewController().push(self)
//                }
                
                let ob = LoginJSON.userInfo()
                if ob?.data?.id != nil {
                    let vc = TabbarVC.viewController()
                    vc.push(self)
                } else {
                    if kAppDelegate.GetTutorialValue() == "1" {
                        let vc = SignInVC.viewController()
                        vc.push(self)
                    } else {
                        let vc = SplashNextVC.viewController()
                        vc.push(self)
                    }
                }
            }
    }
}
