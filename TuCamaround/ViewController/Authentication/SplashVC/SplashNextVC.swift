//
//  SplashNextVC.swift
//  Borobear
//
//  Created by mac on 24/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class SplashNextVC: UIViewController ,  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    static func viewController () -> SplashNextVC {
        return "Authentication".viewController("SplashNextVC") as! SplashNextVC
    }
    @IBOutlet weak var collectionView: UICollectionView! //img: 77
    @IBOutlet weak var btnRstr1: UIButton!
        
    @IBOutlet weak var viewB2: View!
    @IBOutlet weak var viewB3: View!

    @IBOutlet weak var viewNext: View!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view1Shadow: UIView!
    @IBOutlet weak var view2Shadow: UIView!
    @IBOutlet weak var view3Shadow: UIView!
    @IBOutlet weak var constView1: NSLayoutConstraint!
    @IBOutlet weak var constView2: NSLayoutConstraint!
    @IBOutlet weak var constView3: NSLayoutConstraint!

    var currentPage = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPage = 0
       // setPageView(index: currentPage)
        self.btnRstr1.isHidden = false
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionAccess(_ sender: Any) {
        AppActivity.setShowSplash(true)
        SignInVC.viewController().push(self)
    }
    
    @IBAction func actionExplore(_ sender: Any) {
        kAppDelegate.isGuest = "1"
        AppActivity.setShowSplash(true)
        TabbarVC.viewController().push(self)
    }
    
    @IBAction func actionNext(_ sender: Any) {

        if currentPage == 1 {
           AppActivity.setShowSplash(true)
            SignInVC.viewController().push(self)
        }else {
            currentPage = currentPage + 1
            let vclyeee = Int(self.collectionView.bounds.width) * Int(currentPage)
         
            collectionView.scrollRectToVisible(CGRect(x: CGFloat(vclyeee) , y: 0, width: self.collectionView.bounds.width, height: self.collectionView.bounds.height), animated: true)
            setPageView(index: currentPage)
        }

    }
    @IBAction func actionRegister(_ sender: Any) {
        AppActivity.setShowSplash(true)
        let vc = SignInVC.viewController()
       
        self.navigationController?.pushViewController(vc, animated: false)


    }

    func setPageView(index: Int){
        view1Shadow.isHidden = true
        view2Shadow.isHidden = true
        view3Shadow.isHidden = true
        constView1.constant = 12
      //  constView2.constant = 12
        constView3.constant = 12
        
        if index == 0 {
            view1.backgroundColor = appColor.theamOrangeColor
            view2.backgroundColor = appColor.theamLightGrayDark
            view3.backgroundColor = appColor.theamLightGrayDark
            view1Shadow.isHidden = false
            constView1.constant = 26
            
        }/*else if index == 1 {
            view1.backgroundColor = appColor.theamLightGrayDark
            view2.backgroundColor = appColor.theamOrangeColor
            view3.backgroundColor = appColor.theamLightGrayDark
            view2Shadow.isHidden = false
            constView2.constant = 26
            
        }*/else/* if index == 1*/ {
            view1.backgroundColor = appColor.theamLightGrayDark
            view2.backgroundColor = appColor.theamLightGrayDark
            view3.backgroundColor = appColor.theamOrangeColor
            view3Shadow.isHidden = false
            constView3.constant = 26
//            self.viewNext.isHidden = true
//            self.viewB2.isHidden = false
//            self.viewB3.isHidden = false

        }
        
        if currentPage == 0 {
            self.viewNext.isHidden = false
            self.viewB2.isHidden = true
            self.viewB3.isHidden = true
        }else {
            self.viewNext.isHidden = true
            self.viewB2.isHidden = false
            self.viewB3.isHidden = false
        }
        
        
        self.view.layoutIfNeeded()
 
    
    }
    
    var thisWidth:CGFloat = 0
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SplashViewCell", for: indexPath) as? SplashViewCell
        if indexPath.item == 0 {
            cell?.labelTitle.text = "Bienvenido a Camarounds"
            cell?.labelDesc.text = "Contratar servicios nunca fue tan fácil."
         //   cell?.imgView.image = #imageLiteral(resourceName: "Pic_1")
        }else if indexPath.item == 1 {
          /*  cell?.labelTitle.text = "Crea tu solicitud"
            cell?.labelDesc.text = "Elige una categoría, describe lo que \n necesitas y publícalo."
          //  cell?.imgView.image = #imageLiteral(resourceName: "Pic_2")

        }else if indexPath.item == 2 {*/
            cell?.labelTitle.text = "Obtén Solicitudes"
            cell?.labelDesc.text = "Selecciona la mejor oferta para tu solicitud y contrata."
         //   cell?.imgView.image = #imageLiteral(resourceName: "Pic_3")

        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        currentPage = indexPath.item
        setPageView(index: currentPage)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width , height: self.collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

