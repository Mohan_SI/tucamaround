//
//  FPEmailSent.swift
//  PacificPalmsProperty
//
//  Created by mac on 11/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class FPEmailSent: UIViewController {
    static func viewController () -> FPEmailSent {
        return "Authentication".viewController("FPEmailSent") as! FPEmailSent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    

}
