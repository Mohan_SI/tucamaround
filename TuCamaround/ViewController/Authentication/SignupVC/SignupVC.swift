//
//  SignupVC.swift
//  PacificPalmsProperty
//
//  Created by mac on 07/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {
    
    
    static func viewController () -> SignupVC {
        return "Authentication".viewController("SignupVC") as! SignupVC
    }
    
    
    @IBOutlet weak var tfmName: MaterialTextFieldView!
    @IBOutlet weak var tfmComPassword: MaterialTextFieldView!

    @IBOutlet weak var tfmMobile: MaterialTextFieldView!
    @IBOutlet weak var tfmPassword: MaterialTextFieldView!
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfmMobile.keyboardType = UIKeyboardType.emailAddress
        tfmMobile.placeHolder = "Email"
        tfmMobile.delegate = self
        
        tfmPassword.placeHolder = "Password"
        tfmPassword.delegate = self
        tfmPassword.isSecureTextEntry = true
        
        
        tfmName.keyboardType = UIKeyboardType.default
        tfmName.placeHolder = "Full Name"
        tfmName.delegate = self
        
        tfmComPassword.placeHolder = "Confirm Password"
        tfmComPassword.delegate = self
        tfmComPassword.isSecureTextEntry = true
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
      
    @IBAction func actionLogin(_ sender: Any) {
    }
    
    @IBAction func actionCoachSigninup(_ sender: Any) {
        let vc = TermsUse.viewController()
        vc.push(self)
    }
}

extension SignupVC: UITextFieldDelegate, MaterialTextFieldViewDelegate {
    func materialTextChanged(_ textField: UITextField) {
        let superV = (textField.superview as? MaterialTextFieldView)
        superV?.showNoError()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField.superview as? MaterialTextFieldView)?.endEditing()
        materialTextChanged(textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField.superview as? MaterialTextFieldView)?.beginEditing()
    }
    
    
}
