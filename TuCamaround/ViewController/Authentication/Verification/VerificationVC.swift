//
//  VerificationVC.swift
//  TuCamaround
//
//  Created by mac on 15/05/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class VerificationVC: UIViewController {
    
    static func viewController () -> VerificationVC {
        return "Authentication".viewController("VerificationVC") as! VerificationVC
    }
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnCreateAccount: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgCode:UIImageView!

    var strEmail = ""
    var code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCreateAccount.sdBorder()
        
        tfEmail.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        
        lblTitle.text = "Te hemos enviado un código de acceso vía SMS a tu celular \(code) \(strEmail) para completar la verificación"
        wsResendMobile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack (_ sender: Any) {
        self.pop(self)
    }
    
    @IBAction func actionResend(_ sender: Any) {
        wsResendMobile()
    }
    
    @IBAction func actionCreateAccount (_ sender: Any) {
        if valid() {
            wsVerifyMobile()
        }
    }
    
    //MARK: FUNCTIONS
    func valid() -> Bool {
        if tfEmail.text?.count == 0 {
            tfEmail.setError(ValidationClass.blank_code, show: true)
            return false
        } else if tfEmail.text!.count < 6 {
            tfEmail.setError(ValidationClass.invalid_code, show: true)
            return false
        }
        return true
    }
    
    
    //MARK: wsResendMobile
    func wsResendMobile () {
        self.view.endEditing(true)
        
        let param = params()
        param["mobile"] = strEmail
        param["country_code"] = code.replacingOccurrences(of: "+", with: "")

        let httpPara = HttpParams (api_resend_mobile_otp)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsVerifyMobile
    func wsVerifyMobile () {
        self.view.endEditing(true)
        
        let param = params()
        param["code"] = tfEmail.text
        param["mobile"] = strEmail
        param["country_code"] = code.replacingOccurrences(of: "+", with: "")

        let httpPara = HttpParams (api_verify_mobile)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    let ob = json?.data()
                    if ob?.data != nil {
                        ob?.ss?.saveUserInfo()
                    }
                    Toast.toast(string(json!, "message"))
                    self.pop(self)
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension VerificationVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        textField.setError("", show: false)

        var str = ""
        if (isBackSpace == -92) {
            str = textField.text!
            str = String(str.dropLast())
        } else {
            str = textField.text! + string
        }
        
        if str.count == 0 || str.count < 6 {
            changeTint(img: imgCode, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
        } else {
            changeTint(img: imgCode, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
        }
        return (length > 20) ? false : true
    }
}

