//
//  ForgotPassword.swift
//  AMCoaching
//
//  Created by mac on 04/10/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    
    static func viewController () -> ForgotPassword {
        return "Authentication".viewController("ForgotPassword") as! ForgotPassword
    }
    
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var imgEmail: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfEmail.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    //MARK: ACTIONS
    @IBAction func actionReset(_ sender: Any) {
        if valid() {
            wsForgotPassword()
        }
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        self.pop(self)
    }
    
    //MARK: FUNCTIONS
    func valid() -> Bool {
        if tfEmail.text?.count == 0 {
            tfEmail.setError(ValidationClass.blank_email, show: true)
            return false
            
        } else if !validEmail(tfEmail.text!) {
            tfEmail.setError(ValidationClass.invalid_email, show: true)
            return false
        }

        return true
    }
    
    //MARK: wsForgotPassword
    func wsForgotPassword () {
        self.view.endEditing(true)
        
        let param = params()
        param["email"] = tfEmail.text

        let httpPara = HttpParams (api_forgot_password)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                    let vc = ResetPassword.viewController()
                    vc.strEmail = self.tfEmail.text!
                    vc.push(self)
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension ForgotPassword: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        textField.setError("", show: false)
        if (string == " ") {
            return false
        }
        
        var str = ""
        if (isBackSpace == -92) {
            str = textField.text!
            str = String(str.dropLast())
        } else {
            str = textField.text! + string
        }
        
        if !validEmail(str) {
            changeTint(img: imgEmail, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
        } else {
            changeTint(img: imgEmail, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
        }
        return (length > 50) ? false : true
    }
}
