//
//  SignInVC.swift
//  PacificPalmsProperty
//
//  Created by mac on 05/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import Firebase

class SignInVC: UIViewController {
    
    static func viewController () -> SignInVC {
        return "Authentication".viewController("SignInVC") as! SignInVC
    }
    
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var btnPassword: UIButton!
    @IBOutlet var imgEmail: UIImageView!
    @IBOutlet var imgPassword: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tfEmail.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfPassword.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        kAppDelegate.storeTutorialValue("1")
        
        generateAccessToken(completionHandler: { (token) in
        })
        kAppDelegate.isGuest = ""
        statusBarColorChange(color: #colorLiteral(red: 0.9960784314, green: 0.2352941176, blue: 0, alpha: 1))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPassword(_ sender: Any) {
        if tfPassword.isSecureTextEntry == false {
            btnPassword.setImage(UIImage(named: "eye"), for: .normal)
            tfPassword.isSecureTextEntry = true
        } else {
            btnPassword.setImage(UIImage(named: "eyeLine"), for: .normal)
            tfPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func actionForget(_ sender: Any) {
         let vc = ForgotPassword.viewController()
         vc.push(self)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        if valid() {
            wsLogin()
        }
    }
    
    //MARK: FUNCTIONS
    func valid() -> Bool {
        if tfEmail.text?.count == 0 {
            tfEmail.setError(ValidationClass.blank_email, show: true)
            return false
            
        } else if !validEmail(tfEmail.text!) {
            tfEmail.setError(ValidationClass.invalid_email, show: true)
            return false

        } else if tfPassword.text?.count == 0 {
            btnPassword.isHidden = true
            tfPassword.setError(ValidationClass.blank_password, show: true)
            return false

        } else if tfPassword.text!.count < 6 {
            btnPassword.isHidden = true
            tfPassword.setError(ValidationClass.password_length, show: true)
            return false
        }

        return true
    }
    
    //MARK: wsLogin
    func wsLogin () {
        self.view.endEditing(true)
        
        let param = params()
        param["email"] = tfEmail.text
        param["password"] = tfPassword.text

        let httpPara = HttpParams (api_login)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            if json != nil {
                if json!.success() {
                    let ob = json?.data()
                    if ob?.data != nil {
                        ob?.ss?.saveUserInfo()
                    }
                    
                    if let images = json?.object(forKey: "images") as? NSArray {
                        kAppDelegate.storeImages(images.mutableCopy() as! NSMutableArray)
                    }

                    generateAccessToken(completionHandler: { (token) in
                    })
                    
                    let vc = TabbarVC.viewController()
                    vc.push(self)
                } else {
                    Toast.toast(string(json!, "message"))
                }
            }
        }
    }
}

extension SignInVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        textField.setError("", show: false)
        btnPassword.isHidden = false

        var str = ""
        if (isBackSpace == -92) {
            str = textField.text!
            str = String(str.dropLast())
        } else {
            str = textField.text! + string
        }
        
        if textField == tfEmail {
            if (string == " ") {
                return false
            }
            if !validEmail(str) {
                changeTint(img: imgEmail, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgEmail, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
            return (length > 50) ? false : true
        } else {
            if str.count == 0 || str.count < 6 {
                changeTint(img: imgPassword, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgPassword, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
            return (length > 20) ? false : true
        }
    }
}
