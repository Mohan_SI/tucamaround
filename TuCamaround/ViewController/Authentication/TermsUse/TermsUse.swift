//
//  FPEmailSent.swift
//  PacificPalmsProperty
//
//  Created by mac on 11/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class TermsUse: UIViewController {
    static func viewController () -> TermsUse {
        return "Authentication".viewController("TermsUse") as! TermsUse
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    

}
