//
//  ChangePasswordVC.swift
//  AMCoaching
//
//  Created by mac on 14/10/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    static func viewController () -> ChangePasswordVC {
        return "Authentication".viewController("ChangePasswordVC") as! ChangePasswordVC
    }
    
    @IBOutlet var tfOldPassword: UITextField!
    @IBOutlet var tfNewPassword: UITextField!
    @IBOutlet var tfConfirmPassword: UITextField!
    @IBOutlet var btnSave: UIButton!
    
    @IBOutlet var imgOldPassword:UIImageView!
    @IBOutlet var btnOldPassword:UIButton!

    @IBOutlet var imgNewPassword:UIImageView!
    @IBOutlet var btnNewPassword:UIButton!

    @IBOutlet var imgConfirmPassword:UIImageView!
    @IBOutlet var btnConfirmPassword:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        tfOldPassword.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfNewPassword.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfConfirmPassword.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    @IBAction func actionOldPassword(_ sender: Any) {
        if tfOldPassword.isSecureTextEntry == false {
            btnOldPassword.setImage(UIImage(named: "eye"), for: .normal)
            tfOldPassword.isSecureTextEntry = true
        } else {
            btnOldPassword.setImage(UIImage(named: "eyeLine"), for: .normal)
            tfOldPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func actionNewPassword(_ sender: Any) {
        if tfNewPassword.isSecureTextEntry == false {
            btnNewPassword.setImage(UIImage(named: "eye"), for: .normal)
            tfNewPassword.isSecureTextEntry = true
        } else {
            btnNewPassword.setImage(UIImage(named: "eyeLine"), for: .normal)
            tfNewPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func actionConfirmPassword(_ sender: Any) {
        if tfConfirmPassword.isSecureTextEntry == false {
            btnConfirmPassword.setImage(UIImage(named: "eye"), for: .normal)
            tfConfirmPassword.isSecureTextEntry = true
        } else {
            btnConfirmPassword.setImage(UIImage(named: "eyeLine"), for: .normal)
            tfConfirmPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if valid() {
            wsChangePassword()
        }
    }
    
    //MARK: FUNCTIONS
    func valid() -> Bool {
        if tfOldPassword.text?.count == 0 {
            btnOldPassword.isHidden = true
            tfOldPassword.setError(ValidationClass.blank_old_password, show: true)
            return false

        } else if tfOldPassword.text!.count < 6 {
            btnOldPassword.isHidden = true
            tfOldPassword.setError(ValidationClass.password_length, show: true)
            return false
            
        } else if tfNewPassword.text?.count == 0 {
            btnNewPassword.isHidden = true
            tfNewPassword.setError(ValidationClass.blank_new_password, show: true)
            return false

        } else if tfNewPassword.text!.count < 6 {
            btnNewPassword.isHidden = true
            tfNewPassword.setError(ValidationClass.password_length, show: true)
            return false
            
        } else if tfConfirmPassword.text?.count == 0 {
            btnConfirmPassword.isHidden = true
            tfConfirmPassword.setError(ValidationClass.blank_confirm_password, show: true)
            return false

        } else if tfNewPassword.text != tfConfirmPassword.text {
            btnConfirmPassword.isHidden = true
            tfConfirmPassword.setError(ValidationClass.password_mismatch, show: true)
            return false
        }

        return true
    }
    
    //MARK: wsChangePassword
    func wsChangePassword () {
        self.view.endEditing(true)
        
        let param = params()
        param["old_password"] = tfOldPassword.text
        param["new_password"] = tfNewPassword.text
        param["confirm_password"] = tfConfirmPassword.text

        let httpPara = HttpParams (api_change_password)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            //json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    self.pop(self)
                    Toast.toast(string(json!, "message"))
                } else {
                    Toast.toast(string(json!, "message"))
                }
            }
        }
    }
}

extension ChangePasswordVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        textField.setError("", show: false)
        btnOldPassword.isHidden = false
        btnNewPassword.isHidden = false
        btnConfirmPassword.isHidden = false
        
        var str = ""
        if (isBackSpace == -92) {
            str = textField.text!
            str = String(str.dropLast())
        } else {
            str = textField.text! + string
        }
        
        if textField == tfOldPassword {
            if str.count == 0 || str.count < 6 {
                changeTint(img: imgOldPassword, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgOldPassword, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
        } else if textField == tfNewPassword {
            if str.count == 0 || str.count < 6 {
                changeTint(img: imgNewPassword, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgNewPassword, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
        } else {
            if str.count == 0 || tfNewPassword.text != str {
                changeTint(img: imgConfirmPassword, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgConfirmPassword, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
        }
        
        return (length > 20) ? false : true
    }
}
