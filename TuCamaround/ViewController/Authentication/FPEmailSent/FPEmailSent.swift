//
//  FPEmailSent.swift
//  PacificPalmsProperty
//
//  Created by mac on 11/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class PRSuccessfull: UIViewController {
    static func viewController () -> PRSuccessfull {
        return "Authentication".viewController("PRSuccessfull") as! PRSuccessfull
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    

}
