//
//  AppDelegate.swift
//  PacificPalmsProperty
//
//  Created by mac on 05/03/20.
//  Copyright © 2020 mac. All rights reserved.
//
// com.tucamaround


import UIKit
import Firebase
import IQKeyboardManagerSwift
import CoreLocation
import GoogleMaps
import GooglePlacePicker

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    
    var isGuest = ""
    var currentAddress = ""
    var googleApiKey = "AIzaSyCTnBAlPR9sC08OvEsnhU7DFc756RCjSHQ"

    func getLang (_ key: String) -> String {
        return key
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DotLoader.shared.animateDotLoader = true
        IQKeyboardManager.shared.enable = true

        self.window?.makeKeyAndVisible()
        window = UIWindow(frame: UIScreen.main.bounds)
        self.navigationController = UINavigationController(rootViewController: SplashVC.viewController())
        self.navigationController?.isNavigationBarHidden = true
        self.window?.rootViewController = self.navigationController
        //FirebaseApp.configure()
        self.window?.makeKeyAndVisible()
        
        setUpLocation()
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)

        return true
    }

    // MARK: UISceneSession Lifecycle
  /*@available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
*/
    
    func countryCodesArr() -> NSArray {
        do {
            let path = Bundle.main.path(forResource: "country.json", ofType: "")
            let string = try String(contentsOfFile: path!, encoding: .utf8)
            
            return string.json() as! NSArray
        } catch {
            print("text3--")
        }
        
        return NSArray()
    }
    
    func getCurrentCode() ->cCode {
        let arr = countryCodesArr()
        /*let code = cCode()
        code.code = "856" // di.string("phonecode")
        code.icon = UIImage(named: "\("LA".lowercased()).png")
        return code*/
        
        let locale = Locale.current
        for ob in arr {
            let di = ob as! NSDictionary
            if di.string("iso") == locale.regionCode {
                let code = cCode()
                code.code = di.string("phonecode")
                code.icon = UIImage(named: "\(di.string("iso").lowercased()).png")
                return code
                
            }
        }
        return cCode()
    }
    
    func GetTutorialValue() -> String {
        if let data = UserDefaults.standard.object(forKey:"tutorial") as? Data {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? String
            return placesArray!
        }
        return ""
    }
    
    func storeTutorialValue(_ str: String) {
        let userInfoData = NSKeyedArchiver.archivedData(withRootObject: str)
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "tutorial")
        defaults.setValue(userInfoData, forKey: "tutorial")
    }
    
    func storeImages(_ str: NSMutableArray) {
        let userInfoData = NSKeyedArchiver.archivedData(withRootObject: str)
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "img")
        defaults.setValue(userInfoData, forKey: "img")
    }
    
    func GetImage() -> NSMutableArray {
        if let data = UserDefaults.standard.object(forKey:"img") as? Data {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? NSMutableArray
            return placesArray!
        }
        return NSMutableArray()
    }
    
    //MARK:- location
    var locationManager = CLLocationManager()
    func setUpLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if manager.location?.coordinate.latitude != nil {
            getAddressFromGeocodeCoordinate(coordinate: CLLocationCoordinate2D(latitude: manager.location?.coordinate.latitude ?? 0.0, longitude: manager.location?.coordinate.longitude ?? 0.0))
        }
    }
    
    func getAddressFromGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if  response != nil {
                if let address = response!.firstResult() {
                    let lines = address.lines! as [String]
                    if lines.first != "" && lines.first != nil {
                        self.currentAddress = lines.first!
                    }
                }
            }
        }
    }
}
