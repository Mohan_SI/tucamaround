//
//  MyQuotationCell.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class QuotationDetailCell: UITableViewCell {
    
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var lblCommition: UILabel!
    @IBOutlet weak var lblYourQuo: UILabel!
    @IBOutlet weak var lblQutMessage: UILabel!
    @IBOutlet weak var lblQuotTitle: UILabel!
    @IBOutlet weak var lblMateriales: UILabel!
    @IBOutlet weak var btnEditQuotation: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblComment: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
