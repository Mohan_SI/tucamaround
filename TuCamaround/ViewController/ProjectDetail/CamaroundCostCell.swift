//
//  CamaroundCostCell.swift
//  TuCamaround
//
//  Created by mac on 26/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol CamaroundCostDelegate {
    func actionCostShow()
    func actionSubmitRating(rate_one: Double, rate_two: Double, rate_three: Double, comment: String)
}

class CamaroundCostCell: UITableViewCell , UITextViewDelegate {
    @IBOutlet weak var viewGiveRatting: UIView!
    @IBOutlet weak var viewAllRatting: UIView!
    @IBOutlet weak var viewYourRatting: UIView!
    @IBOutlet weak var viewUserRatting: UIView!
    @IBOutlet weak var viewPaymentStatus: UIView!
    @IBOutlet weak var viewCostDetail: UIView!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var lblPlaceeholder: UILabel!
    @IBOutlet weak var tvMesage: UITextView!

    @IBOutlet weak var lblQuotTitle: UILabel!

    @IBOutlet weak var lblCommition: UILabel!
    @IBOutlet weak var lblYourQuo: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMateriales: UILabel!

    @IBOutlet weak var lblPayment1: UILabel!
    @IBOutlet weak var lblPayment1Status: UILabel!
    
    @IBOutlet weak var lblPayment2: UILabel!
    @IBOutlet weak var lblPayment2Status: UILabel!

    
    @IBOutlet weak var lblName: UILabel!

    @IBOutlet weak var lblUserRatting: UILabel!
    @IBOutlet weak var userRatting: FloatRatingView!
    
    @IBOutlet weak var lblUserRatDate: UILabel!
    @IBOutlet weak var btnUser: UIButton!
    
    @IBOutlet weak var lblMyRatting: UILabel!
    
    @IBOutlet weak var lblMyRattingDate: UILabel!
    
    @IBOutlet weak var btnMyRate: UIButton!
    @IBOutlet weak var btnVew: UIButton!
    @IBOutlet weak var myRatting: FloatRatingView!
    @IBOutlet weak var lblRattingName: UILabel!
    
    @IBOutlet weak var puntualityRat: FloatRatingView!
    @IBOutlet weak var amabilidadRat: FloatRatingView!
    @IBOutlet weak var exactitudRat: FloatRatingView!
    @IBOutlet weak var btnSubmitRatting: UIButton!
    
    var delegate:CamaroundCostDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tvMesage.delegate = self
        // Initialization code
        viewPaymentStatus.isHidden = true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        lblPlaceeholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.count == 0 {
            lblPlaceeholder.isHidden = false
        }else {
            lblPlaceeholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            if textView.text == "" || textView.text.count == 0 {
                lblPlaceeholder.isHidden = false
            }else {
                lblPlaceeholder.isHidden = true
            }
         }
        return true
    }
    
    /* Older versions of Swift */
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            if textView.text == "" || textView.text.count == 0 {
                lblPlaceeholder.isHidden = false
            }else {
                lblPlaceeholder.isHidden = false
            }
         }
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionDropDown(_ sender: UIButton) {
        delegate.actionCostShow()
    }
    
    @IBAction func actionSubmitRating(_ sender: UIButton) {
        delegate.actionSubmitRating(rate_one: puntualityRat.rating, rate_two: amabilidadRat.rating, rate_three: exactitudRat.rating, comment: tvMesage.text)
    }
    
    func setDetail(showCostDetail:Bool, giveRating:Bool, allRating:Bool, userRating:Bool, selfRating:Bool)  {
        viewCostDetail.isHidden = true
        viewGiveRatting.isHidden = true
        viewAllRatting.isHidden = true
        viewUserRatting.isHidden = true
        viewYourRatting.isHidden = true

        if showCostDetail {
            viewCostDetail.isHidden = false
            imgDropDown.image = #imageLiteral(resourceName: "dropdown")
            
            if giveRating {
                viewGiveRatting.isHidden = false
            }
            
            if allRating {
                viewAllRatting.isHidden = false
                
                if userRating {
                    viewUserRatting.isHidden = false
                }
                
                if selfRating {
                    viewYourRatting.isHidden = false
                }
            }
            
        } else {
            imgDropDown.image = #imageLiteral(resourceName: "up1")
        }
    }
}
