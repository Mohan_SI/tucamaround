//
//  QuotationAcceptCell.swift
//  TuCamaround
//
//  Created by mac on 26/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol QuotationAcceptDelegate {
    func actionAccept()
    func actionReject()
}

class QuotationAcceptCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    var delegate:QuotationAcceptDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: ACTIONS
    @IBAction func actionAccept(_ sender: Any) {
        delegate.actionAccept()
    }
    
    @IBAction func actionReject(_ sender: Any) {
        delegate.actionReject()
    }
}
