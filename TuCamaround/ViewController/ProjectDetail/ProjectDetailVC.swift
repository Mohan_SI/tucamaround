//
//  ProjectDetailVC.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ProjectDetailVC: UIViewController {
    
    static func viewController () -> ProjectDetailVC {
        return "Home".viewController("ProjectDetailVC") as! ProjectDetailVC
    }
     
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewPopupReject: UIView!
    @IBOutlet weak var lblTitle: UILabel!

    //var projectOB : ProjectObject!
    var showMyRatting = false
    var showDetail = true
    var showCost = true
    var showCostDetail = true
    var showMyFeedback = false
    var dataImage = [PostImageData]()
    var dataDetail:PostDetailData!
    var dataQuestion = [AddonsData]()
    var showShimmer = true
    var dataBidData: PostBidData!
    var dataJobReivew:ProJobReviewsData?
    var dataUserReview:UserReviewsData?
    var dictJob = NSMutableDictionary()
    var dataUserDetails:UserDetailsData?
    var commisionValue:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.showLoader()
        
        lblTitle.text = string(dictJob, "name")
        
        wsAdminCharges()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCancelReject(_ sender: Any) {
        viewPopupReject.removeFromSuperview()
    }
    
    @IBAction func actionConfirmCancel(_ sender: Any) {
        viewPopupReject.removeFromSuperview()
        wsAccepDecline("declined")
    }
    
    //MARK: FUNCTIONS
    func addPopupReject() {
        viewPopupReject.frame = self.view.frame
        self.view.addSubview(viewPopupReject)
    }
    
    //MARK: wsAdminCharges
    func wsAdminCharges () {
        self.view.endEditing(true)

        let param = params()

        let httpPara = HttpParams (api_admin_charges)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = false

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    if let data = json?.object(forKey: "data") as? NSDictionary {
                        self.commisionValue = number(data, "commision").intValue
                    }
                } else {
                    Http.alert("", string(json!, "message"))
                }
                self.wsPostDetails()
            } else {
                self.wsPostDetails()
            }
        }
    }
    
    //MARK: wsPostDetails
    func wsPostDetails () {
        self.view.endEditing(true)

        let param = params()
        param["job_id"] = string(dictJob, "id")
        param["user_id"] = string(dictJob, "user_id")
        
        let httpPara = HttpParams (api_post_details)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = false

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            self.view.hideLoader()
            self.showShimmer = false
            
            if json != nil {
                if json!.success() {
                    self.tblView.isHidden = false
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(PostDetailJSON.self, from: data)
                            if let data = jsonn.data {
                                self.dataDetail = data
                            }
                            
                            if let addons = jsonn.addons {
                                self.dataQuestion = addons
                            }
                            
                            if let images = jsonn.images {
                                self.dataImage = images
                            }
                            
                            if let postBidData = jsonn.postBidData {
                                self.dataBidData = postBidData
                            }
                            
                            if let proReview = jsonn.proJobReviews {
                                self.dataJobReivew = proReview
                            }
                            
                            if let userReview = jsonn.userReviews {
                                self.dataUserReview = userReview
                            }
                            
                            if let userDetails = jsonn.userDetails {
                                self.dataUserDetails = userDetails
                            }
                            
                            self.tblView.reloadData()
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    Http.alert("", string(json!, "message"))
                }
            } else {
                self.tblView.isHidden = true
            }
        }
    }
    
    //MARK: wsPostBid
    func wsPostBid (_ amount: String, _ days: String, _ comment: String, _ material: String, _ totalAmount: String, _ percent: String) {
        self.view.endEditing(true)

        let param = params()
        param["job_id"] = string(dictJob, "id")
        param["job_amount"] = totalAmount
        param["working_days"] = days
        param["comment"] = comment
        param["material_include"] = material
        param["user_id"] = dataDetail.userId
        param["admin_commision"] = amount
        param["admin_percent"] = percent

        if dataBidData != nil {
            if dataBidData.id != nil {
                param["action"] = "edit"
                param["bid_id"] = dataBidData.id
            } else {
                param["action"] = "add"
            }
        } else{
            param["action"] = "add"
        }

        let httpPara = HttpParams (api_post_bid)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsAccepDecline
    func wsAccepDecline (_ action: String) {
        self.view.endEditing(true)

        let param = params()
        param["bid_id"] = dataBidData.id
        param["action"] = action
        param["job_id"] = string(dictJob, "id")

        let httpPara = HttpParams (api_accept_declined_request)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsTechRating
    func wsTechRating (rate_one: Double, rate_two: Double, rate_three: Double, comment: String) {
        self.view.endEditing(true)

        let param = params()
        param["job_id"] = string(dictJob, "id")
        param["user_id"] = string(dictJob, "user_id")
        param["rate_one"] = rate_one
        param["rate_two"] = rate_two
        param["rate_three"] = rate_three
        param["comment"] = comment

        let httpPara = HttpParams (api_tech_give_review)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                    self.wsPostDetails()
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension ProjectDetailVC: UITableViewDelegate, UITableViewDataSource, QuotationRequestDelegate, QuotationAcceptDelegate, CamaroundCostDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        if dataBidData != nil {
            if dataBidData.bidStatus?.lowercased() == "award" {
                return 3
            } else if dataBidData.bidStatus?.lowercased() == "approve" || dataDetail.bookingStatus?.lowercased() == "completed" {
                return 2
            }
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataBidData != nil {
            if dataBidData.bidStatus?.lowercased() == "award" {
                if section == 0 {
                    return 1
                } else if section == 1 {
                    if !showDetail {
                         return 1
                    }
                    return dataQuestion.count
                } else {
                    return 1
                }
            } else if dataBidData.bidStatus?.lowercased() == "approve" || dataDetail.bookingStatus?.lowercased() == "completed" {
                if section == 0 {
                    if !showDetail {
                         return 1
                    }
                    return dataQuestion.count
                } else {
                    return 1
                }
            }
        }
        
        if section == 0 {
            if !showDetail {
                 return 1
            }
            return dataQuestion.count
        } else {
            return 1
        }
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataBidData != nil {
            if dataBidData.bidStatus?.lowercased() == "award" {
                if indexPath.section == 0 {
                    let cell =  Bundle.main.loadNibNamed("QuotationAcceptCell", owner: self, options: nil)?.first as! QuotationAcceptCell
                    
                    cell.lblPayment.text = dataBidData.paymentType?.capFirstLetter()
                    
                    if let amount = dataBidData.jobAmount {
                        if amount != "" {
                            cell.lblTitle.text = "Francis te ha adjudicado este Camaround por  \(Int(amount)!.withCommas()) USD"
                        }
                    }
                    
                    
                    cell.delegate = self
                    
                    return cell
                    
                } else if indexPath.section == 1 {
                    let cell =  Bundle.main.loadNibNamed("CamaroundDetailsCell", owner: self, options: nil)?.first as! CamaroundDetailsCell
                    cell.btnShowDetail.addTarget(self, action: #selector(actionDetail(btn:)), for: .touchUpInside)
                    
                    if dataDetail != nil {
                        cell.lblAddress.text = dataDetail.address
                        
                        cell.dataImage = dataImage
                        cell.dataQuestion = dataQuestion
                        cell.setDetail(showDetail,index: indexPath.row)
                        
                        cell.btnViewMap.isHidden = true
                        cell.consHtBtnMap.constant = 0
                        
                        if let userid = dataDetail.userId {
                            cell.userId = userid
                        }
                    }
                    
                    if dataUserDetails != nil {
                        cell.lblName.text = (dataUserDetails?.firstName?.capFirstLetter() ?? "") + " " + (dataUserDetails?.lastName?.capFirstLetter() ?? "")
                        cell.lblRatting.text = dataUserDetails?.rating
                        
                        if let rating = dataUserDetails?.rating {
                            if rating != "" {
                                cell.rating.rating = Double(rating)!
                            }
                        }
                        
                        if let img = dataUserDetails?.profileImage {
                            cell.imgUser.sd_setImage(with: URL(string: img.profilePic(dataUserDetails?.id ?? "")), placeholderImage: UIImage(named: "def"))
                        }
                        cell.lblTotalRating.text = "\(dataUserDetails?.totalReviews ?? "") Valoraciones"

                    }
                    return cell
                    
                } else {
                    let cell =  Bundle.main.loadNibNamed("QuotationDetailCell", owner: self, options: nil)?.first as! QuotationDetailCell
                    
                    if let commision = dataBidData.adminCommision {
                        if commision != "" {
                            cell.lblAmount.text = (Int(commision)!.withCommas())
                        }
                    }
                    
                    if let percent = dataBidData.adminPercent {
                        if percent != "" {
                            cell.lblCommition.text = (Int(percent)!.withCommas())
                        }
                    }
                    
                    if let amount = dataBidData.jobAmount {
                        if amount != "" {
                            cell.lblYourQuo.text = (Int(amount)!.withCommas())
                        }
                    }
                    
                    cell.lblDays.text = (dataBidData.workingDays ?? "") + " días"
                    cell.lblComment.text = dataBidData.comment
                    
                    if dataBidData.materialInclude?.lowercased() == "yes" {
                        cell.lblMateriales.isHidden = false
                    } else {
                        cell.lblMateriales.isHidden = true
                    }
                    return cell
                }
                
            } else if dataBidData.bidStatus?.lowercased() == "approve" || dataDetail.bookingStatus?.lowercased() == "completed" {
                
                if indexPath.section == 0 {
                    let cell =  Bundle.main.loadNibNamed("CamaroundDetailsCell", owner: self, options: nil)?.first as! CamaroundDetailsCell
                    cell.btnShowDetail.addTarget(self, action: #selector(actionDetail(btn:)), for: .touchUpInside)
                    cell.btnViewMap.addTarget(self, action: #selector(actionViewMap(btn:)), for: .touchUpInside)

                    if dataDetail != nil {
                        cell.lblAddress.text = dataDetail.address
                        
                        cell.dataImage = dataImage
                        cell.dataQuestion = dataQuestion
                        cell.setDetail(showDetail,index: indexPath.row)
                        
                        if dataDetail.bookingStatus?.lowercased() == "completed" {
                             cell.btnViewMap.isHidden = true
                             cell.consHtBtnMap.constant = 0
                        } else {
                             cell.btnViewMap.isHidden = false
                             cell.consHtBtnMap.constant = 32
                        }
                        
                        if let userid = dataDetail.userId {
                            cell.userId = userid
                        }
                    }
                    
                    if dataUserDetails != nil {
                        cell.lblName.text = (dataUserDetails?.firstName?.capFirstLetter() ?? "") + " " + (dataUserDetails?.lastName?.capFirstLetter() ?? "")
                        cell.lblRatting.text = dataUserDetails?.rating
                        
                        if let rating = dataUserDetails?.rating {
                            if rating != "" {
                                cell.rating.rating = Double(rating)!
                            }
                        }
                        
                        if let img = dataUserDetails?.profileImage {
                            cell.imgUser.sd_setImage(with: URL(string: img.profilePic(dataUserDetails?.id ?? "")), placeholderImage: UIImage(named: "def"))
                        }
                        
                        cell.lblTotalRating.text = "\(dataUserDetails?.totalReviews ?? "") Valoraciones"

                    }
                    
                    return cell
                    
                } else {
                    let cell =  Bundle.main.loadNibNamed("CamaroundCostCell", owner: self, options: nil)?.first as! CamaroundCostCell
                    
                    if let commision = dataBidData.adminCommision {
                        if commision != "" {
                            cell.lblPrice.text = (Int(commision)!.withCommas())
                        }
                    }
                    
                    if let percent = dataBidData.adminPercent {
                        if percent != "" {
                            cell.lblCommition.text = (Int(percent)!.withCommas())
                        }
                    }
                    
                    if let amount = dataBidData.jobAmount {
                        if amount != "" {
                            cell.lblYourQuo.text = (Int(amount)!.withCommas())
                        }
                    }
                    
                    cell.lblDays.text = (dataBidData.workingDays ?? "") + " días"
                    
                    if dataBidData.materialInclude?.lowercased() == "yes" {
                        cell.lblMateriales.isHidden = false
                    } else {
                        cell.lblMateriales.isHidden = true
                    }

                    if dataDetail.bookingStatus?.lowercased() == "completed" {
                        if dataUserReview?.id != nil && dataUserReview?.id != "" {
                            cell.setDetail(showCostDetail: showCost, giveRating: true, allRating: true, userRating: true, selfRating: false)
                            
                            cell.lblName.text = "Calificación de \(dataUserReview?.firstName?.capFirstLetter() ?? "")"
                            cell.lblUserRatting.text = dataUserReview?.average
                            
                            cell.lblRattingName.text = "Califica a \(dataUserReview?.firstName ?? "")"

                            if let rating = dataUserReview?.average {
                                if rating != "" {
                                    cell.userRatting.rating = Double(rating)!
                                }
                            }
                            
                            let date = dataUserReview?.created?.getDate("yyyy-MM-dd")
                            if date != nil {
                                cell.lblUserRatDate.text = "\(date!.getStringDate("dd")) \(date!.getStringDate("MMM").getDtt()) \(date!.getStringDate("yyyy"))"
                            }
                        }
                        
                        if dataJobReivew?.id != "" {
                            cell.setDetail(showCostDetail: showCost, giveRating: false, allRating: true, userRating: true, selfRating: true)
                            cell.lblMyRatting.text = dataJobReivew?.average
                            if let rating = dataJobReivew?.average {
                                if rating != "" {
                                    cell.myRatting.rating = Double(rating)!
                                }
                            }
                            
                            let date = dataJobReivew?.created?.getDate("yyyy-MM-dd")
                            if date != nil {
                                cell.lblMyRattingDate.text = "\(date!.getStringDate("dd")) \(date!.getStringDate("MMM").getDtt()) \(date!.getStringDate("yyyy"))"
                            }
                            
                        } else {
                            cell.setDetail(showCostDetail: showCost, giveRating: true, allRating: true, userRating: true, selfRating: false)
                        }
                    } else {
                        cell.setDetail(showCostDetail: showCost, giveRating: false, allRating: false, userRating: false, selfRating: false)
                    }
                    
                    cell.delegate = self
                    
                    return cell
                }
            }
        }
        
        if indexPath.section == 0 {
            let cell =  Bundle.main.loadNibNamed("CamaroundDetailsCell", owner: self, options: nil)?.first as! CamaroundDetailsCell
            cell.btnShowDetail.addTarget(self, action: #selector(actionDetail(btn:)), for: .touchUpInside)
            
            if dataDetail != nil {
                cell.lblAddress.text = dataDetail.address
                
                cell.dataImage = dataImage
                cell.dataQuestion = dataQuestion
                cell.setDetail(showDetail,index: indexPath.row)
                
                cell.btnViewMap.isHidden = true
                cell.consHtBtnMap.constant = 0
                
                if let userid = dataDetail.userId {
                    cell.userId = userid
                }
            }
            
            if dataUserDetails != nil {
                cell.lblName.text = (dataUserDetails?.firstName?.capFirstLetter() ?? "") + " " + (dataUserDetails?.lastName?.capFirstLetter() ?? "")
                cell.lblRatting.text = dataUserDetails?.rating
                
                if let rating = dataUserDetails?.rating {
                    if rating != "" {
                        cell.rating.rating = Double(rating)!
                    }
                }
                
                if let img = dataUserDetails?.profileImage {
                    cell.imgUser.sd_setImage(with: URL(string: img.profilePic(dataUserDetails?.id ?? "")), placeholderImage: UIImage(named: "def"))
                }
                
                cell.lblTotalRating.text = "\(dataUserDetails?.totalReviews ?? "") Valoraciones"
            }
            return cell
            
        } else {
            let cell =  Bundle.main.loadNibNamed("QuotationRequestCell", owner: self, options: nil)?.first as! QuotationRequestCell
            
            if commisionValue != 0 {
                cell.lblCommition.text = commisionValue.withCommas()
            }
            
            cell.commisionValue = commisionValue
            
            if dataBidData != nil {
                if dataBidData.id != nil {
                    cell.tfDays.text = dataBidData.workingDays
                    cell.tfPrice.text = dataBidData.adminCommision
                    if dataBidData.materialInclude?.lowercased() == "yes" {
                        cell.actionYes(self)
                    } else {
                        cell.actionNo(self)
                    }
                    cell.tvMesage.text = dataBidData.comment
                    cell.lblPlaceeholder.isHidden = true
                    cell.btnSubmitQoutation.setTitle("Modificar Cotización", for: .normal)
                    
                    if let adminpercent = dataBidData.adminPercent {
                        if adminpercent != "" {
                            cell.lblCommition.text = (Int(adminpercent)!.withCommas())
                        }
                    }
                    
                    if let amount = dataBidData.jobAmount {
                        if amount != "" {
                            cell.lblYourQuo.text = (Int(amount)!.withCommas())
                        }
                    }
                    
                    cell.commisionValue = Int(dataBidData.adminPercent ?? "") ?? 0
                }
            }
            
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func actionSubmit(_ amount: String, _ days: String, _ comment: String, material_include: String, _ totalAmount: String, _ percent: String) {
        self.view.endEditing(true)
        if amount.count == 0 {
            Toast.toast(ValidationClass.blank_amount)
        } else if days.count == 0 {
            Toast.toast(ValidationClass.blank_days)
        } else if comment.count == 0 {
            Toast.toast(ValidationClass.blank_comments)
        } else {
            wsPostBid(amount, days, comment, material_include, totalAmount, percent)
        }
    }
    
    @objc func actionSaveRatting(btn:UIButton) {
        showMyRatting = true
        tblView.reloadData()
    }
    
    @objc func actionDetail(btn:UIButton) {
        showDetail = !showDetail
        tblView.reloadData()
    }
    
    @objc func actionViewMap(btn:UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:"comgooglemaps://?center=\(dataDetail.latitude ?? ""),\(dataDetail.longitude ?? "")&zoom=14&views=traffic")!)
        } else {
            print("Can't use comgooglemaps://")
        }
    }
    
    func actionCostShow() {
        showCost = !showCost
        tblView.reloadData()
    }
    
    func actionSubmitRating(rate_one: Double, rate_two: Double, rate_three: Double, comment: String) {
        wsTechRating(rate_one: rate_one, rate_two: rate_one, rate_three: rate_three, comment: comment)
    }
    
    @objc func actionRepeat(btn:UIButton) {
        showCostDetail = !showCostDetail
        tblView.reloadData()
    }
    
    func actionAccept() {
        wsAccepDecline("approve")
    }
    
    func actionReject() {
        addPopupReject()
    }
}

struct PostDetailJSON: Codable {
    let status: String?
    let data  : PostDetailData?
    let message : String?
    let addons: [AddonsData]?
    let images: [PostImageData]?
    let postBidData: PostBidData?
    let postBidLogData: [PostBidLogData]?
    let proJobReviews:ProJobReviewsData?
    let userReviews:UserReviewsData?
    let userDetails:UserDetailsData?

    enum CodingKeys: String, CodingKey {
        case status, data, message, addons, images
        case postBidData = "post_bid_data"
        case postBidLogData = "post_bid_logs"
        case proJobReviews = "pro_job_reviews"
        case userReviews = "user_reviews"
        case userDetails = "user_details"
    }
}

class PostDetailData: Codable {

    let address: String?
    let bookingAmount: String?
    let bookingStatus: String?
    let categoryId: String?
    let country: String?
    let createdBy: String?
    let createdOn: String?
    let id: String?
    let isRated: String?
    let latitude: String?
    let longitude: String?
    let paymentStatus: String?
    let paymentType: String?
    let postNumber: String?
    let serviceDetails: String?
    let status: String?
    let subcategoryId: String?
    let techId: String?
    let totalAmount: String?
    let updatedBy: String?
    let updatedOn: String?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case address, country, id, latitude, longitude, status
        case bookingAmount = "booking_amount"
        case bookingStatus = "booking_status"
        case categoryId = "category_id"
        case createdBy = "created_by"
        case createdOn = "created_on"
        case isRated = "is_rated"
        case paymentStatus = "payment_status"
        case paymentType = "payment_type"
        case postNumber = "post_number"
        case serviceDetails = "service_details"
        case subcategoryId = "subcategory_id"
        case techId = "tech_id"
        case totalAmount = "total_amount"
        case updatedBy = "updated_by"
        case updatedOn = "updated_on"
        case userId = "user_id"
    }
}

class AddonsData: Codable {

    let answer: String?
    let categoryId: String?
    let describes: String?
    let id: String?
    let postId: String?
    let questionId: String?
    let questionTitle: String?
    let serviceId: String?

    enum CodingKeys: String, CodingKey {
        case answer, describes, id
        case categoryId = "category_id"
        case postId = "post_id"
        case questionId = "question_id"
        case questionTitle = "question_title"
        case serviceId = "service_id"
    }
}

class PostImageData: Codable {

    let id: String?
    let image: String?
    let postId: String?

    enum CodingKeys: String, CodingKey {
        case id, image
        case postId = "post_id"
    }
}

class PostBidData: Codable {

    let bidStatus: String?
    let comment: String?
    let createdOn: String?
    let firstName: String?
    let id: String?
    let jobAmount: String?
    let jobId: String?
    let lastName: String?
    let materialInclude: String?
    let paymentType: String?
    let profileImage: String?
    let rating: String?
    let techId: String?
    let updatedOn: String?
    let userId: String?
    let workingDays: String?
    let adminCommision: String?
    let adminPercent: String?

    enum CodingKeys: String, CodingKey {
        case comment, id, rating
        case bidStatus = "bid_status"
        case createdOn = "created_on"
        case firstName = "first_name"
        case jobAmount = "job_amount"
        case jobId = "job_id"
        case lastName = "last_name"
        case materialInclude = "material_include"
        case paymentType = "payment_type"
        case profileImage = "profile_image"
        case techId = "tech_id"
        case updatedOn = "updated_on"
        case userId = "user_id"
        case workingDays = "working_days"
        case adminCommision = "admin_commision"
        case adminPercent = "admin_percent"
    }
}

class PostBidLogData: Codable {

    let createdOn: String?
    let id: String?
    let jobAmounts: String?
    let jobId: String?
    let status: String?
    let techId: String?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case id, status
        case createdOn = "created_on"
        case jobAmounts = "job_amounts"
        case jobId = "job_id"
        case techId = "tech_id"
        case userId = "user_id"
    }
}

class ProJobReviewsData: Codable {

    let average: String?
    let comment: String?
    let commonId: String?
    let createdBy: String?
    let created: String?
    let firstName: String?
    let id: String?
    let orderId: String?
    let profileImage: String?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case average, comment, id, created
        case commonId = "common_id"
        case createdBy = "created_by"
        case firstName = "first_name"
        case orderId = "order_id"
        case profileImage = "profile_image"
        case userId = "user_id"
    }
}

class UserReviewsData: Codable {

    let average: String?
    let comment: String?
    let commonId: String?
    let createdBy: String?
    let created: String?
    let firstName: String?
    let id: String?
    let orderId: String?
    let profileImage: String?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case average, comment, id, created
        case commonId = "common_id"
        case createdBy = "created_by"
        case firstName = "first_name"
        case orderId = "order_id"
        case profileImage = "profile_image"
        case userId = "user_id"
    }
}


class UserDetailsData: Codable {

    let firstName: String?
    let lastName: String?
    let profileImage: String?
    let rating: String?
    let id: String?
    let totalReviews: String?

    enum CodingKeys: String, CodingKey {
        case rating, id
        case firstName = "first_name"
        case lastName = "last_name"
        case profileImage = "profile_image"
        case totalReviews = "total_reviews"
    }
}

extension String {
    func getDtt() -> String {
        if self == "Jan" {
            return "Ene"
            
        } else if self == "Apr" {
            return "Abr"

        } else if self == "Aug" {
            return "Ago"

        } else {
            return self
        }
    }
}
