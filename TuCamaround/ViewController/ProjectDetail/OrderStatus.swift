//
//  OrderStatus.swift
//  TuCamaround
//
//  Created by mac on 26/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

 
enum OrderStatus : Int {
    case pending = 0
    case awarded = 1
    case inProgress = 2
   // case accepted = 3
    case inReview = 4
    case complete = 5
    case retting = 6
    case cancelled = 7
}
