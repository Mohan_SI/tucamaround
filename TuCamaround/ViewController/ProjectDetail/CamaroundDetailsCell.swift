//
//  CamaroundDetailsCell.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class CamaroundDetailsCell: UITableViewCell {
    @IBOutlet weak var viewUserInfo: UIView!
    @IBOutlet weak var viewimageInfo: UIView!
    @IBOutlet weak var viewQuesAns: UIView!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var btnShowDetail: UIButton!
    @IBOutlet weak var viewAddress: UIView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRatting: UILabel!
    @IBOutlet weak var lblTotalRating: UILabel!
    @IBOutlet weak var lblQues: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgDropDown: UIImageView!

    @IBOutlet weak var clnView: UICollectionView!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var lblAddress: UILabel!

    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet var consHtBtnMap: NSLayoutConstraint!
    
    var dataQuestion = [AddonsData]()
    var dataImage = [PostImageData]()
    var userId = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        clnView.register(UINib(nibName: "ProjectImageCell", bundle: nil), forCellWithReuseIdentifier: "ProjectImageCell")
        clnView.delegate = self
        clnView.dataSource = self
        
        btnViewMap.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setDetail(_ hideDetail: Bool, index:Int )  {
        if !hideDetail {
            viewQuesAns.isHidden = true
            viewimageInfo.isHidden = true
            viewUserInfo.isHidden = true
            viewAddress.isHidden = true
            imgDropDown.image = #imageLiteral(resourceName: "up1")
            if index != 0 {
                viewDetail.isHidden = true
            }else {
                viewDetail.isHidden = false
            }
        }else {
            imgDropDown.image = #imageLiteral(resourceName: "dropdown")
            if index == 0 {
                viewDetail.isHidden = false
                viewUserInfo.isHidden = false
                viewQuesAns.isHidden = false
                viewimageInfo.isHidden = true
                viewAddress.isHidden = true

            }else if index == dataQuestion.count - 1{
                viewDetail.isHidden = true
                viewQuesAns.isHidden = false
                viewimageInfo.isHidden = false
                viewUserInfo.isHidden = true
                viewAddress.isHidden = false
                
                if dataImage.count == 0 {
                    viewimageInfo.isHidden = true
                } else {
                    viewimageInfo.isHidden = false
                }
                
            }else {
                viewDetail.isHidden = true
                viewQuesAns.isHidden = false
                viewimageInfo.isHidden = true
                viewUserInfo.isHidden = true
                viewAddress.isHidden = true
            }
        }
        
        let ob = dataQuestion[index]
        lblQues.text = ob.questionTitle
        lblAns.text = ob.answer
    }
}

extension CamaroundDetailsCell: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, ProjectImageDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectImageCell", for: indexPath) as? ProjectImageCell
        
        let ob = dataImage[indexPath.row]
        
        if let img = ob.image {
            cell?.imgIcon.sd_setImage(with: URL(string: img.devicePic(userId)), placeholderImage: UIImage(named: ""))
        }
        
        cell?.delegate = self
        cell?.index = indexPath
        
        return cell!
    }
    
    func actionImgPreview(_ index: IndexPath) {
//        let ob = dataImage[index.row]
//
//        let previewImage = Bundle.main.loadNibNamed("PreviewImage", owner:
//                self, options: nil)?.first as? PreviewImage
//        kAppDelegate.window?.addSubview(previewImage!)
//            
//        previewImage?.imgVw.enableZoom()
//        if let img = ob.image {
//            previewImage?.imgVw.sd_setImage(with: URL(string: img.devicePic(userId)), placeholderImage: UIImage(named: ""))
//        }
//        
//        previewImage?.frame = CGRect(x:0, y: 0, width: (kAppDelegate.window?.bounds.width)!, height: (kAppDelegate.window?.bounds.height)!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
