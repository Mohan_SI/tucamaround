//
//  ProjectImageCell.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol ProjectImageDelegate {
    func actionImgPreview(_ index:IndexPath)
}

class ProjectImageCell: UICollectionViewCell {
    @IBOutlet weak var imgIcon: UIImageView!

    var delegate: ProjectImageDelegate!
    var index: IndexPath!
    
    @IBAction func actionImgPreview(_ sender: Any) {
        delegate.actionImgPreview(index)
    }
}
