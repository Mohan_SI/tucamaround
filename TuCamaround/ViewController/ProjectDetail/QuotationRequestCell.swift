//
//  QuotationDetailsCell.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol QuotationRequestDelegate {
    func actionSubmit(_ amount: String, _ days: String, _ comment: String, material_include: String, _ totalAmount: String, _ percent: String)
}

class QuotationRequestCell: UITableViewCell, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var lblCommition: UILabel!
    @IBOutlet weak var lblYourQuo: UILabel!
    @IBOutlet weak var tfDays: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tvMesage: UITextView!
    @IBOutlet weak var lblPlaceeholder: UILabel!
    @IBOutlet weak var lblQuotTitle: UILabel!
    @IBOutlet weak var btnMatYes: UIButton!
    @IBOutlet weak var btnMatNo: UIButton!
    @IBOutlet weak var btnSubmitQoutation: UIButton!

    var delegate:QuotationRequestDelegate!
    var include = "yes"
    var commisionValue:Int = 0
    var totalValue:Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        setTF(tfDays, str: "Dias")
        setTF(tfPrice, str: "USD")
    }
    
    @IBAction func actionYes(_ sender: Any) {
        include = "yes"
        btnMatYes.setImage(UIImage(named: "check"), for: .normal)
        btnMatNo.setImage(UIImage(named: "uncheck"), for: .normal)
    }
    
    @IBAction func actionNo(_ sender: Any) {
        include = "no"
        btnMatYes.setImage(UIImage(named: "uncheck"), for: .normal)
        btnMatNo.setImage(UIImage(named: "check"), for: .normal)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        delegate.actionSubmit(tfPrice.text!, tfDays.text!, tvMesage.text!, material_include: include, "\(totalValue)", "\(commisionValue)")
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength: Int = textField.text!.count + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if textField == tfPrice {
            
            let validCharacterSet = NSCharacterSet(charactersIn: "1234567890").inverted
            let filter = string.components(separatedBy: validCharacterSet)
            
            if filter.count == 1 {
                if newLength > 0 {
                    if (isBackSpace == -92) {
                        var str = textField.text!
                        str = String(str.dropLast())
                        totalValue = (commisionValue + Int(str)!)
                        lblYourQuo.text = "\(totalValue.withCommas())"
                    } else {
                        totalValue = commisionValue + Int(textField.text! + string)!
                        lblYourQuo.text = "\(totalValue.withCommas())"
                    }
                    return true
                } else {
                    lblYourQuo.text = "$0.00"
                }
            } else {
                return false
            }
        }
        
        return true
    }
    
    //MARK: TEXTVIEW DELEGATE
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblPlaceeholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.count == 0 {
            lblPlaceeholder.isHidden = false
        }else {
            lblPlaceeholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            if textView.text == "" || textView.text.count == 0 {
                lblPlaceeholder.isHidden = false
            }else {
                lblPlaceeholder.isHidden = true
            }
        }
        return true
    }
    
    func setTF(_ tf:UITextField , str:String){
        tf.placeholder = str

        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 76, height: 46))
        paddingView.backgroundColor = .red
        paddingView.backgroundColor = .clear
        let lblTitle = UILabel(frame: CGRect(x: 1, y: 6, width: 60, height: 30))
        lblTitle.text = str
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont(name: AppFont.Regular, size: CGFloat(16))
        lblTitle.textColor = appColor.theamLightGrayDark
        //lblTitle.textColor = appColor.black
        paddingView.addSubview(lblTitle)
        
        let lineView1 = UIView(frame: CGRect(x: 1, y: 10, width: 1, height: 26))
        lineView1.backgroundColor = appColor.theamLightGrayDark
        paddingView.addSubview(lineView1)
        
        tf.rightView = paddingView
        tf.rightViewMode = .always
        tf.border(#colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1), 8, 1)
        tf.paddingL(10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
