//
//  UserInfoVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos

protocol ProfileWorkDoneDelegate {
    func actionDeletePhotos(_ index: IndexPath)
}

class ProfileWorkDoneCell: UICollectionViewCell {
    @IBOutlet var imgPhoto: UIImageView!
    
    var delegate:ProfileWorkDoneDelegate!
    var index:IndexPath!
    
    @IBAction func actionDeletePhotos(_ sender: Any) {
        delegate.actionDeletePhotos(index)
    }
}

class CountryListCell: UITableViewCell {
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgRadio: UIImageView!
}

class UserInfoVC: UIViewController {

    @IBOutlet var tfName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var tfCode: UITextField!
    @IBOutlet var imgName: UIImageView!
    @IBOutlet var imgEmail: UIImageView!
    @IBOutlet var imgPhone: UIImageView!
    @IBOutlet var imgPhoneCheck: UIImageView!

    @IBOutlet var viewPopupCountry: UIView!
    @IBOutlet var tblCountry: UITableView!
    
    @IBOutlet var btnChangePassword: UIButton!
    @IBOutlet var cvPhotos: UICollectionView!

    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var imgCode: UIImageView!
    @IBOutlet var tvAbout: TextView!
    @IBOutlet var viewDot: UIView!
    @IBOutlet var viewAdd: UIView!

    var profilePic: UIImage? = nil
    var dataCountry = [CountryListData]()
    var selectInt:Int = -1
    var arrImg = [UIImage]()
    var maImg = NSMutableArray()
    var check_green = UIImage(named: "check_green")
    var uncheck_green = UIImage(named: "Group 16115")
    
    static func viewController () -> UserInfoVC {
        return "Home".viewController("UserInfoVC") as! UserInfoVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfName.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfEmail.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfMobile.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        
        btnChangePassword.setAttributedTitle(getAtr(txt: "Cambia la contraseña", clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1)), for: .normal)

        setData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imgPhoneCheck.addGestureRecognizer(tap)
        imgPhoneCheck.isUserInteractionEnabled = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if tfMobile.text?.count != 0 {
            let vc = VerificationVC.viewController()
            vc.strEmail = tfMobile.text!
            vc.code = lblCode.text!
            vc.push(self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let borderLayer = viewDot.dashedBorderLayerWithColor(UIColor.hexColor(0xE0E2E6).cgColor)
        viewDot.layer.addSublayer(borderLayer)
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = #colorLiteral(red: 0.8784313725, green: 0.8862745098, blue: 0.9019607843, alpha: 1)
        yourViewBorder.lineDashPattern = [8, 4]
        yourViewBorder.frame = viewAdd.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.lineWidth = 1
        yourViewBorder.cornerRadius = 6
        yourViewBorder.path = UIBezierPath(rect: viewAdd.bounds).cgPath
        viewAdd.layer.addSublayer(yourViewBorder)
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    @IBAction func actionSelectCountry(_ sender: Any) {
        //addPopupCountry()
        self.view.endEditing (true)
        CountryPickerView.instance.present { (obj) in
            self.imgCode.image = UIImage(named: "\(obj.string("iso").lowercased()).png")
            self.lblCode.text = "+" + obj.string("phonecode")
        }
    }
    
    @IBAction func actionCountryCross(_ sender: Any) {
        viewPopupCountry.removeFromSuperview()
    }
    
    @IBAction func actionCountrySubmit(_ sender: Any) {
        if selectInt == -1 {
            Toast.toast(ValidationClass.select_country)
        } else {
            let ob = dataCountry[selectInt]
            tfCode.text = "+" + ob.phoCode!
            viewPopupCountry.removeFromSuperview()
        }
    }
    
    @IBAction func actionEdit(_ sender: Any) {
        changeProfilePicture()
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if validate() {
            wsUpdateProfile()
        }
    }
    
    @IBAction func actionChangePassword(_ sender: Any) {
        let vc = ChangePasswordVC.viewController()
        vc.push(self)
    }
    
    @IBAction func actionImgSelect(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: FUNCTIONS
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var image = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 80, height: 80), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                image = result!
                arrayOfImages.append(image)
            })
        }

        return arrayOfImages
    }
    
    func getAssetThumbnail1(assets: [PHAsset]) -> NSMutableArray {
        let arrayOfImages = NSMutableArray()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var image = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 80, height: 80), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                image = result!
                
                let dict = NSMutableDictionary()
                dict["isSelect"] = "1"
                dict["img"] = image
                arrayOfImages.add(dict)
            })
        }

        return arrayOfImages
    }
    
    func getAtr(txt: String, clr:UIColor) -> NSMutableAttributedString {
        let fontStyle = UIFont(name: "Rubik-Regular", size: 16.0)!
        
        let atr = NSMutableAttributedString(string: txt, attributes: [NSAttributedString.Key.foregroundColor: clr,NSAttributedString.Key.font : fontStyle, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        let final = NSMutableAttributedString()
        final.append(atr)
        
        return final
    }
    
    func addPopupCountry() {
        self.view.endEditing(true)
        viewPopupCountry.frame = kAppDelegate.window!.frame
        //kAppDelegate.window?.addSubview(viewPopupCountry)
        self.view.addSubview(viewPopupCountry)
    }
    
    func setData() {
        changeTint(img: imgName, clr: #colorLiteral(red: 0.1004235372, green: 0.1446691453, blue: 0.2420555651, alpha: 1))
        changeTint(img: imgEmail, clr: #colorLiteral(red: 0.1004235372, green: 0.1446691453, blue: 0.2420555651, alpha: 1))
        changeTint(img: imgPhone, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
        
        let ob = LoginJSON.userInfo()
        tfName.text = ob?.data?.firstName.capFirstLetter()
        tfEmail.text = ob?.data?.email
        tfMobile.text = ob?.data?.mobileNo
        tvAbout.text = ob?.data?.aboutus
        
        viewDot.isHidden = false
        cvPhotos.isHidden = true
        viewAdd.isHidden = true

        if kAppDelegate.GetImage().count != 0 {
            viewDot.isHidden = true
            cvPhotos.isHidden = false
            viewAdd.isHidden = false
            maImg = kAppDelegate.GetImage()
            cvPhotos.reloadData()
        }
        
        
        if let img = ob?.data?.profileImage {
            imgProfile.sd_setImage(with: URL(string: img.profilePic((ob?.data?.id)!)), placeholderImage: UIImage(named: "def"))
        }
        
        if ob?.data?.nonverifyMobile.count == 0 {
            changeImg(img: imgPhoneCheck, imgCheck: check_green!)
        } else {
            changeImg(img: imgPhoneCheck, imgCheck: uncheck_green!)
        }
        
        if let code = ob?.data?.countryCode {
            lblCode.text = "+" + code
            let arr = kAppDelegate.countryCodesArr()
            for ob in arr {
                let di = ob as! NSDictionary
                if di.string("phonecode") == code {
                    imgCode.image = UIImage(named: "\(di.string("iso").lowercased()).png")
                    return
                }
            }
        }
        
        //wsCountryList()
    }
    
    func validate() -> Bool {
        if tfName.text?.count == 0 {
            tfName.setError(ValidationClass.blank_name, show: true)
            return false

        } else if tfEmail.text?.count == 0 {
            tfEmail.setError(ValidationClass.blank_email, show: true)
            return false
            
        } else if !validEmail(tfEmail.text!) {
            tfEmail.setError(ValidationClass.invalid_email, show: true)
            return false

        } else if tfMobile.text?.count == 0 {
            tfMobile.setError(ValidationClass.blank_cell_phone, show: true)
            return false

        } else if tfMobile.text!.count < 4 {
            tfMobile.setError(ValidationClass.cell_phone_length, show: true)
            return false
            
        }
        /*else if tfCode.text?.count == 0 {
            self.view.endEditing(true)
            Toast.toast(ValidationClass.select_country_code)
            return false
        }*/
        
        return true
    }
    
    //MARK: wsCountryList
    func wsCountryList () {

        let httpPara = HttpParams (api_get_country)
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(CountryListJSON.self, from: data)
                            if let data = jsonn.data {
                                self.dataCountry = data
                                self.tblCountry.reloadData()
                                
                                let obj = LoginJSON.userInfo()
                                for i in 0..<data.count {
                                    let ob = data[i]
                                    if ob.phoCode == obj?.data?.countryCode {
                                        self.selectInt = i
                                        break
                                    }
                                }
                                self.tblCountry.reloadData()
                            }
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    
                }
            }
        }
    }
    
    //MARK: wsUpdateProfile
    func wsUpdateProfile () {
        self.view.endEditing(true)

        let param = params()
        param["name"] = tfName.text
        param["email"] = tfEmail.text
        param["mobile"] = tfMobile.text
        param["country_code"] = lblCode.text?.replacingOccurrences(of: "+", with: "")
        param["aboutus"] = tvAbout.text

        let ma = NSMutableArray()
        
        if maImg.count != 0 {
            for i in 0..<maImg.count {
                let dd = maImg.object(at: i) as! NSDictionary
                if string(dd, "isSelect") == "1" {
                    let md = NSMutableDictionary()
                    md["param"] = "images[]"
                    md["image"] = dd.object(forKey: "img") as? UIImage
                    ma.add(md)
                }
            }
        } else {
            for i in 0..<arrImg.count {
                let md = NSMutableDictionary()
                md["param"] = "images[]"
                md["image"] = arrImg[i]
                ma.add(md)
            }
        }
        
        let httpPara = HttpParams (api_user_update_profile)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true
        httpPara.images = ma

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    self.pop(self)
                    Toast.toast(string(json!, "messgae"))
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsProfileImg
    func wsProfileImg () {
        self.view.endEditing(true)

        let param = params()
        let ma = NSMutableArray()
        if profilePic != nil  {
            let md = NSMutableDictionary()
            md["param"] = "image"
            md["image"] = profilePic!
            ma.add(md)
        }
        
        let httpPara = HttpParams (api_user_profilepic)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true
        httpPara.images = ma

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsProfileImg
    func wsDeleteImg (image_id: String, old_file: String, index: Int) {
        self.view.endEditing(true)

        let param = params()
        param["image_id"] = image_id
        param["old_file"] = old_file
        
        let httpPara = HttpParams (api_delete_images)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    //self.dataImages.remove(at: index)
                    self.maImg.removeObject(at: index)
                    self.cvPhotos.reloadData()
                    
                    if self.maImg.count == 0 {
                        self.viewDot.isHidden = false
                        self.cvPhotos.isHidden = true
                        self.viewAdd.isHidden = true
                    }
                    Toast.toast(string(json!, "message"))
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension UserInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, OpalImagePickerControllerDelegate, ProfileWorkDoneDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if maImg.count != 0 {
            return maImg.count
        } else {
            return arrImg.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileWorkDoneCell", for: indexPath) as! ProfileWorkDoneCell
        let obLogin = LoginJSON.userInfo()

        cell.imgPhoto.layer.cornerRadius = 6.0
        
        if maImg.count != 0 {
            let dict = maImg.object(at: indexPath.row) as! NSDictionary
            
            if string(dict, "isSelect") == "1" {
                cell.imgPhoto.image = dict.object(forKey: "img") as? UIImage
            } else {
                print(string(dict, "image").workDone((obLogin?.data?.id)!))
                cell.imgPhoto.sd_setImage(with: URL(string: string(dict, "image").workDone((obLogin?.data?.id)!)), placeholderImage: UIImage(named: ""))
            }
            
        } else {
            cell.imgPhoto.image = arrImg[indexPath.row]
        }
        
        cell.delegate = self
        cell.index = indexPath

        return cell
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        self.viewDot.isHidden = true
        cvPhotos.isHidden = false
        viewAdd.isHidden = false
        
        if maImg.count != 0 {
            let arr = self.getAssetThumbnail1(assets: assets)
            for i in 0..<arr.count {
                let dict = arr.object(at: i) as! NSDictionary
                maImg.add(dict)
            }
        } else {
            let arr = getAssetThumbnail(assets: assets)
            for i in 0..<arr.count {
                arrImg.append(arr[i])
            }
        }
        
        self.cvPhotos.reloadData()
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        if arrImg.count == 0 && maImg.count == 0 {
            self.viewDot.isHidden = false
        } else {
            self.viewDot.isHidden = true
        }
    }
    
    func actionDeletePhotos(_ index: IndexPath) {
        if maImg.count != 0 {
            let dict = maImg.object(at: index.row) as! NSDictionary
            if string(dict, "isSelect") == "1" {
                maImg.removeObject(at: index.row)
                cvPhotos.reloadData()
                if self.maImg.count == 0 {
                    viewDot.isHidden = false
                    cvPhotos.isHidden = true
                    viewAdd.isHidden = true
                }
            } else {
                wsDeleteImg(image_id: string(dict, "id"), old_file: string(dict, "image"), index: index.row)
            }
        } else {
            self.arrImg.remove(at: index.row)
            self.cvPhotos.reloadData()
            
            if self.arrImg.count == 0 {
                viewDot.isHidden = false
                cvPhotos.isHidden = true
                viewAdd.isHidden = true
            }
        }
        
        /*let alert = UIAlertController(title: "Confirmación", message: "¿Estás segura de que quieres eliminar?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(void) in

        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: {(void) in
        }))
        self.present(alert, animated: true, completion: nil)*/
    }
}

extension UserInfoVC: UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        textField.setError("", show: false)

        var str = ""
        if (isBackSpace == -92) {
            str = textField.text!
            str = String(str.dropLast())
        } else {
            str = textField.text! + string
        }
        
        if textField == tfName {
            if str.count == 0 {
                changeTint(img: imgName, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgName, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
            return (length > 60) ? false : true
            
        } else if textField == tfEmail {
            if !validEmail(str) {
                changeTint(img: imgEmail, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgEmail, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
            return (length > 60) ? false : true
            
        } else if textField == tfMobile {
            if str.count == 0 || str.count < 4 {
                changeTint(img: imgPhone, clr: #colorLiteral(red: 0.8235294118, green: 0.8431372549, blue: 0.8823529412, alpha: 1))
            } else {
                changeTint(img: imgPhone, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
            }
            return (length > 16) ? false : true
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension UserInfoVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image: UIImage?
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = pickedImage
        } else if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = pickedImage
        }
        let data  =  image!.jpegData(compressionQuality: 0.3 )
        image = UIImage(data: data!)
        imgProfile.image = image
        profilePic = image
        wsProfileImg()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera() {
        let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func changeProfilePicture() {
        let myalert = UIAlertController(title: "Elegir opcion", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        myalert.addAction(UIAlertAction(title: "Cámara", style: .default) { (action:UIAlertAction!) in
            self.openCamera()
        })
        
        myalert.addAction(UIAlertAction(title: "Galería", style: .default) { (action:UIAlertAction!) in
            self.openGallary()
        })
        
        myalert.addAction(UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in
        })
        
        self.present(myalert, animated: true)
    }
}

extension UserInfoVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCountry {
            return dataCountry.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCountry {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListCell", for: indexPath) as! CountryListCell
            
            let ob = dataCountry[indexPath.row]
            
            cell.lblName.text = ob.countryName! + " +(\(ob.phoCode!)" + ")"
            
            if selectInt == indexPath.row {
                cell.imgRadio.image = UIImage(named: "orange_circle")
                cell.lblName.textColor = #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1)
            } else {
                cell.imgRadio.image = UIImage(named: "radio_uncheck")
                cell.lblName.textColor = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblCountry {
            selectInt = indexPath.row
            tableView.reloadData()
        }
    }
}

struct CountryListJSON: Codable {
    let status: String?
    let data  : [CountryListData]?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case status, data, message
    }
}

class CountryListData: Codable {

    let conCode: String?
    let countryName: String?
    let createdBy: String?
    let createdOn: String?
    let phoCode: String?
    let status: String?
    let id: String?

    enum CodingKeys: String, CodingKey {
        case id, status
        case conCode = "con_code"
        case countryName = "country_name"
        case createdBy = "created_by"
        case createdOn = "created_on"
        case phoCode = "pho_code"
    }
}

extension String {

    func stringToImage(_ handler: @escaping ((UIImage?)->())) {
        if let url = URL(string: self) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = data {
                    let image = UIImage(data: data)
                    handler(image)
                }
            }.resume()
        }
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}
