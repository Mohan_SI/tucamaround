//
//  BankDetailVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class BankDetailVC: UIViewController {
    
    @IBOutlet weak var tfmName: UITextField!
    @IBOutlet weak var tfmEmail: UITextField!
    @IBOutlet weak var tfmMobile: UITextField!
    
    static func viewController () -> BankDetailVC {
        return "Home".viewController("BankDetailVC") as! BankDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfmName.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfmEmail.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfmMobile.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        self.view.endEditing(true)
        self.actionBack(1)
    }
}

extension BankDetailVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

