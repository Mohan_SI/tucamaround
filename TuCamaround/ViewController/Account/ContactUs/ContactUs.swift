//
//  ContactUs.swift
//  TuCamarounds
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos

class ContactUs: UIViewController {

    static func viewController () -> ContactUs {
        return "Settings".viewController("ContactUs") as! ContactUs
    }
    
    @IBOutlet var tfConsultation: UITextField!
    @IBOutlet var tfRefNo: UITextField!
    @IBOutlet var tvHelp: TextView!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var imgConsultation: UIImageView!
    @IBOutlet var imgRefNo: UIImageView!
    @IBOutlet var viewPhotos: UIView!
    @IBOutlet var viewDorB0order: UIView!
    @IBOutlet var cvPhotos: UICollectionView!
    @IBOutlet var viewAdd: UIView!
    @IBOutlet var consHtVwAdd: NSLayoutConstraint!
    @IBOutlet var lblPlaceeholder: UILabel!
    
    @IBOutlet var viewPopupPkr: UIView!
    @IBOutlet var pkrVw: UIPickerView!
    @IBOutlet var viewEffect: UIVisualEffectView!

    var profilePic: UIImage? = nil
    var dataConsultation = [HelpTopicData]()
    var dataRefNo = [LastOrderData]()
    var strSelect = ""
    var selectConsultation:HelpTopicData?
    var selectRefNo:LastOrderData?
    var arrImg = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        tfConsultation.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfRefNo.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        btnSend.sdBorder()

        wsHelpList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            let borderLayer  = self.viewDorB0order.dashedBorderLayerWithColor(UIColor.hexColor(0xE0E2E6).cgColor)
            self.viewDorB0order.layer.addSublayer(borderLayer)
            
            let yourViewBorder = CAShapeLayer()
            yourViewBorder.strokeColor = #colorLiteral(red: 0.8784313725, green: 0.8862745098, blue: 0.9019607843, alpha: 1)
            yourViewBorder.lineDashPattern = [8, 4]
            yourViewBorder.frame = self.viewAdd.bounds
            yourViewBorder.fillColor = nil
            yourViewBorder.lineWidth = 1
            yourViewBorder.cornerRadius = 6
            yourViewBorder.path = UIBezierPath(rect: self.viewAdd.bounds).cgPath
            self.viewAdd.layer.addSublayer(yourViewBorder)
        }
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    @IBAction func actionConsultation(_ sender: Any) {
        strSelect = "1"
        addPopup()
    }
    
    @IBAction func actionRefNo(_ sender: Any) {
        strSelect = "2"
        addPopup()
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func actionSend(_ sender: Any) {
        if let msg = checkValidation() {
            Toast.toast(msg)
        } else {
            wsContactUs()
        }
    }
    
    @IBAction func actionCancelPkr(_ sender: Any) {
        viewPopupPkr.removeFromSuperview()
    }
    
    @IBAction func actionDonePkr(_ sender: Any) {
        if strSelect == "1" {
            tfConsultation.text = selectConsultation?.headingOne
            changeTint(img: imgConsultation, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
        } else {
            tfRefNo.text = selectRefNo?.postNumber
            changeTint(img: imgRefNo, clr: #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1))
        }
        viewPopupPkr.removeFromSuperview()
    }
    
    //MARK: FUNCTIONS
    func addPopup() {
        self.view.endEditing(true)
        viewEffect.alpha = 0.5
        viewPopupPkr.frame = self.view.frame
        self.view.addSubview(viewPopupPkr)
        
        pkrVw.reloadAllComponents()
        pkrVw.selectRow(0, inComponent: 0, animated: true)
    }
    
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var image = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 80, height: 80), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                image = result!
                arrayOfImages.append(image)
            })
        }

        return arrayOfImages
    }
    
    func checkValidation() -> String? {
        self.view.endEditing(true)
        if tfConsultation.text?.count == 0 {
            return ValidationClass.select_consultation
        } else if tfRefNo.text?.count == 0 {
            return ValidationClass.select_reference_no
        } else if tvHelp.text.count == 0 {
            return ValidationClass.blank_comment
        }
        return nil
    }
    
    //MARK: wsHelpList
    func wsHelpList () {
        self.view.endEditing(true)

        let param = params()
        
        let httpPara = HttpParams (api_help_booking_list)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(ContactUsJSON.self, from: data)
                            
                            if let helptopics = jsonn.helptopics {
                                self.dataConsultation = helptopics
                            }
                            
                            if let lastOrder = jsonn.lastOrder {
                                self.dataRefNo = lastOrder
                            }
                            
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
    
    //MARK: wsContactUs
    func wsContactUs () {
        self.view.endEditing(true)

        let param = params()
        param["booking_id"] = selectRefNo?.id
        param["enquiry_type"] = selectConsultation?.id
        param["details"] = tvHelp.text

        let ma = NSMutableArray()
        for i in 0..<arrImg.count {
            let md = NSMutableDictionary()
            md["param"] = "image[]"
            md["image"] = arrImg[i]
            ma.add(md)
        }
        
        let httpPara = HttpParams (api_help_support_input)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true
        httpPara.images = ma

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)

            if json != nil {
                if json!.success() {
                    Toast.toast(string(json!, "message"))
                    let vc = ContactUsSuccess.viewController()
                    vc.push(self)
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension ContactUs: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblPlaceeholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.count == 0 {
            lblPlaceeholder.isHidden = false
        }else {
            lblPlaceeholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            if textView.text == "" || textView.text.count == 0 {
                lblPlaceeholder.isHidden = false
            }else {
                lblPlaceeholder.isHidden = true
            }
        }
        return true
    }
}

extension ContactUs: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ContactUs: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if strSelect == "1" {
            return dataConsultation.count
        } else {
            return dataRefNo.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if strSelect == "1" {
            let ob = dataConsultation[row]
            return ob.headingOne
        } else {
            let ob = dataRefNo[row]
            return ob.postNumber
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if strSelect == "1" {
            let ob = dataConsultation[row]
            selectConsultation = ob
        } else {
            let ob = dataRefNo[row]
            selectRefNo = ob
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var strTitle = ""
        
        if strSelect == "1" {
            let ob = dataConsultation[row]
            strTitle = ob.headingOne ?? ""
            selectConsultation = ob
        } else {
            let ob = dataRefNo[row]
            strTitle = ob.postNumber ?? ""
            selectRefNo = ob
        }
        
        let myTitle = NSAttributedString(string: strTitle, attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
        return myTitle
    }
}

extension ContactUs: UICollectionViewDelegate, UICollectionViewDataSource, OpalImagePickerControllerDelegate, ProfileWorkDoneDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileWorkDoneCell", for: indexPath) as! ProfileWorkDoneCell
        
        cell.imgPhoto.layer.cornerRadius = 6
        cell.imgPhoto.image = arrImg[indexPath.row]
        
        cell.delegate = self
        cell.index = indexPath

        return cell
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        viewDorB0order.isHidden = true
        cvPhotos.isHidden = false
        viewAdd.isHidden = false
        consHtVwAdd.constant = 122

        let arr = getAssetThumbnail(assets: assets)
        for i in 0..<arr.count {
            arrImg.append(arr[i])
        }
        
        self.cvPhotos.reloadData()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        if arrImg.count == 0  {
            viewDorB0order.isHidden = false
            consHtVwAdd.constant = 170
        } else {
            viewDorB0order.isHidden = true
            consHtVwAdd.constant = 122
        }
    }
    
    func actionDeletePhotos(_ index: IndexPath) {
        arrImg.remove(at: index.row)
        cvPhotos.reloadData()
        
        if self.arrImg.count == 0 {
            viewDorB0order.isHidden = false
            cvPhotos.isHidden = true
            viewAdd.isHidden = true
            consHtVwAdd.constant = 170
        }
    }
}

struct ContactUsJSON: Codable {
    let status: String?
    let helptopics: [HelpTopicData]?
    let message: String?
    let lastOrder: [LastOrderData]?

    enum CodingKeys: String, CodingKey {
        case status, message, helptopics
        case lastOrder = "last_order"
    }
}

class HelpTopicData: Codable {

    let createdBy: String?
    let createdOn: String?
    let decpOne: String?
    let descpTwo: String?
    let headingOne: String?
    let headingTwo: String?
    let id: String?
    let status: String?
    let type: String?

    enum CodingKeys: String, CodingKey {
        case id, status, type
        case createdBy  = "created_by"
        case createdOn  = "created_on"
        case decpOne  = "decp_one"
        case descpTwo  = "descp_two"
        case headingOne  = "heading_one"
        case headingTwo  = "heading_two"
    }
}

class LastOrderData: Codable {

    let bookingStatus: String?
    let categoryId: String?
    let id: String?
    let postNumber: String?
    let totalAmount: String?

    enum CodingKeys: String, CodingKey {
        case id
        case bookingStatus  = "booking_status"
        case categoryId  = "category_id"
        case postNumber  = "post_number"
        case totalAmount  = "total_amount"
    }
}
