//
//  ContactUsSuccess.swift
//  TuCamarounds
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
 
class ContactUsSuccess: UIViewController {

    static func viewController () -> ContactUsSuccess {
        return "Settings".viewController("ContactUsSuccess") as! ContactUsSuccess
    }
    
    @IBOutlet var btnReturn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        let arr = self.navigationController?.viewControllers
        if arr != nil {
            for vc in arr! {
                if vc is TabbarVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    
    @IBAction func actionReturn(_ sender: Any) {
        let arr = self.navigationController?.viewControllers
        if arr != nil {
            for vc in arr! {
                if vc is TabbarVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
}
