//
//  FrequentQuestions.swift
//  TuCamarounds
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class FrequentQuestionsCell: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
}

class FrequentQuestions: UIViewController {

    static func viewController () -> FrequentQuestions {
        return "Settings".viewController("FrequentQuestions") as! FrequentQuestions
    }
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var btnContactUs: UIButton!
    
    var arrData = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        wsFrequent()
     }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
    
    @IBAction func actionContactUs(_ sender: Any) {
        let vc = ContactUs.viewController()
        vc.push(self)
    }
    
    //MARK: wsFrequent
    func wsFrequent () {
        self.view.endEditing(true)

        let param = params()
        
        let httpPara = HttpParams (api_help_support)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            if json != nil {
                if json!.success() {
                    if let data = json?.object(forKey: "data") as? NSArray {
                        for i in 0..<data.count {
                            let dict = data.object(at: i) as! NSDictionary
                            if let helptypes = dict.object(forKey: "helptypes") as? NSArray {
                                for j in 0..<helptypes.count {
                                    let dd = helptypes.object(at: j) as! NSDictionary
                                    self.arrData.add(dd)
                                }
                            }
                        }
                        self.tblView.reloadData()
                    }
                } else {
                    Http.alert("", string(json!, "message"))
                }
            } else {
                self.tblView.reloadData()
            }
        }
    }
}

extension FrequentQuestions: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FrequentQuestionsCell", for: indexPath) as! FrequentQuestionsCell
        
        let dict = arrData.object(at: indexPath.row) as! NSDictionary
        cell.lblTitle.text = string(dict, "question").capFirstLetter()
        
        return cell
    }
}
