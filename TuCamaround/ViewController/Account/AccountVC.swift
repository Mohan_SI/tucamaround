//
//  AccountVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SDWebImage

class AccountVC: UIViewController {
    
    static func viewController () -> ProjectsVC {
        return "Home".viewController("ProjectsVC") as! ProjectsVC
    }
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var sc: UIScrollView!
    @IBOutlet weak var viewRating:FloatRatingView!
    @IBOutlet weak var lblRating:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let attributedText = NSMutableAttributedString(string: "Mostrar perfil", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        lblProfile.attributedText = attributedText
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.actionProfile))
        lblProfile.addGestureRecognizer(tap)
        lblProfile.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if kAppDelegate.isGuest == "1" {
            sc.isHidden = true
        } else {
            sc.isHidden = false
            wsGetProfile()
        }
    }
    
    //MARK: ACTIONS
    @objc func actionProfile(_ sender: UITapGestureRecognizer) {
        Profile.viewController().push(self)
    }
    
    @IBAction func actionWallet(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            MyBalanceVC.viewController().push(self)
        }
    }
    
    @IBAction func actionInfo(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            UserInfoVC.viewController().push(self)
        }
    }

    @IBAction func actionBank(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            BankDetailVC.viewController().push(self)
        }
    }
    
    @IBAction func actionNotificatio(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            NotificationVC.viewController().push(self)
        }
    }
    
    @IBAction func actionQuestion(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            FrequentQuestions.viewController().push(self)
        }
    }
    
    @IBAction func actionContUs(_ sender: UIButton) {
        if kAppDelegate.isGuest != "1" {
            ContactUs.viewController().push(self)
        }
    }
    
    @IBAction func actionAbout(_ sender: UIButton) {
        AboutCamarounds.viewController().push(self)
    }
    
    @IBAction func actionTerm(_ sender: UIButton) {
        TermsUse.viewController().push(self)
    }
    
    @IBAction func actionLogout(_ sender: UIButton) {
        let alert = UIAlertController(title: "Confirmación", message: "¿Estás seguro de que quieres cerrar sesión?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(void) in
            removeToken()
            
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "img")
            
            LoginJSON.logout()
            self.tabBarController?.tabBar.isHidden = true
            let vc = SplashVC.viewController()
            vc.push(self)
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: {(void) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: FUNCTIONS
    func setData() {
        let ob = LoginJSON.userInfo()
        lblName.text = ob?.data?.firstName.capFirstLetter()
        if let img = ob?.data?.profileImage {
            imgUser.sd_setImage(with: URL(string: img.profilePic((ob?.data?.id)!)), placeholderImage: UIImage(named: "def"))
        }
        
        lblRating.text = ob?.data?.rating
        if let rating = ob?.data?.rating {
            if rating != "" {
                viewRating.rating = Double(rating)!
            }
        }
    }
    
    //MARK: wsGetProfile
    func wsGetProfile () {
        self.view.endEditing(true)
        
        let param = params()

        let httpPara = HttpParams (api_get_my_profile)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    let ob = json?.data()
                    if ob?.data != nil {
                        ob?.ss?.saveUserInfo()
                    }
                    
                    if let images = json?.object(forKey: "images") as? NSArray {
                        kAppDelegate.storeImages(images.mutableCopy() as! NSMutableArray)
                    }
                    
                    self.setData()
                } else {
                    Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}
