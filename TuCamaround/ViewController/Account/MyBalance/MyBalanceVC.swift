//
//  MyBalanceVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MyBalanceVC: UIViewController {
    static func viewController () -> MyBalanceVC {
        return "Home".viewController("MyBalanceVC") as! MyBalanceVC
    }
    
    let viewShimmer = ShimmerHomeView.instance.showShimmer()
    var issimmer = true
    @IBOutlet weak var tblView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var arrSelected:[Int] = []
    var arrList : [[String:String]] = [
        ["title":"Air Conditioning"],
        ["title":"Basement"],
        ["title":"Bath"],
        ["title":"Built-ins"],
        ["title":"Dishwasher"],
        ["title":"Fully Fenced"],
        ["title":"Furnished"],
        ["title":"Gas Enabled"],
        ["title":"Internal Laundry"],
        ["title":"Lift"],
        ["title":"Living Area"],
        ["title":"Polished"],
    ]
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.estimatedRowHeight = 200.0
        self.tblView.rowHeight = UITableView.automaticDimension
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)
        
        // Do any additional setup after loading the view.
        
        let deadline = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.viewShimmer.removeFromSuperview()
            
            self.issimmer = false
            
            self.tblView.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    @objc func refresh(sender:AnyObject) {
        
        //        apiForOrderList()
        self.issimmer = true
        
        
        self.tblView.reloadData()
        let deadline = DispatchTime.now() + .seconds(3)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refreshControl.endRefreshing()
            self.issimmer = false
            DispatchQueue.main.async {
                self.viewShimmer.removeFromSuperview()
                
                self.tblView.reloadData()
            }
        }
    }
    
    //MARK: - Navigation
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionBal(_ sender: Any) {
        WalletPaymentVC.viewController().push(self)
    }
}


extension MyBalanceVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if issimmer {
            tblView.addSubview(self.viewShimmer)
            return 0
        }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBalanceCell") as!  MyBalanceCell
 
        cell.setDetail(indexPath.row)
        
        return cell
        
    }
    
    
  
}

