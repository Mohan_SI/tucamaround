//
//  MyBalanceCell.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MyBalanceCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblBala: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDetail(_ index :Int )  {
        lblName.text = "Thomas Reed"
        lblDate.text = "18/08/2019"
        lblPrice.text = "$80.00"
        lblBala.text = "Bal - $80.00"
        lblTitle.text = "Pago Camaround"
        
        if index%2 == 0  {
            lblPrice.textColor = appColor.black
            imgIcon.image  = #imageLiteral(resourceName: "minus")
            lblBala.textColor = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
        } else {
            
            lblPrice.textColor = appColor.theamGreenColor
            lblBala.textColor = appColor.theamGreenColor
            if index == 1 {
                lblPrice.textColor = appColor.theamOrangeColor
            }
            imgIcon.image  = #imageLiteral(resourceName: "Plus")
        }
    }
}
