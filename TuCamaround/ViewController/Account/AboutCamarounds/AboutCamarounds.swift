//
//  AboutCamarounds.swift
//  TuCamarounds
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class AboutCamarounds: UIViewController {

    static func viewController () -> AboutCamarounds {
        return "Settings".viewController("AboutCamarounds") as! AboutCamarounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }
}
