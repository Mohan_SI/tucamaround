//
//  WalletPaymentVC.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class WalletPaymentVC: UIViewController {
    
    static func viewController () -> WalletPaymentVC {
        return "Home".viewController("WalletPaymentVC") as! WalletPaymentVC
    }
    
    @IBOutlet weak var btnCard: UIButton!
    @IBOutlet weak var btnBank: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var tfPrice: UITextField!

    var selectCartType = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitle.isHidden = true
        tfPrice.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if selectCartType {
            
            CardDetailVC.viewController().push(self)
        }else {
            
       //     BankTransVC.viewController().push(self)
        }
      //  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBank(_ sender: Any) {
        btnSave.setTitle(" Foto de Comprobante ", for: .normal)
        lblTitle.text = "Ten en cuenta que los pagos por transferencia bancaria pueden tardar 24-48 horas."
        viewTitle.isHidden = false
        selectCartType = false
        lblPrice.text = "Seleccione una imagen"
        btnCard.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        btnBank.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        btnCard.setTitleColor(appColor.theamGrayDark , for: .normal)
        btnBank.setTitleColor(appColor.black , for: .normal)
    }
    
    @IBAction func actionCard(_ sender: Any) {
        btnSave.setTitle("    Pagar    ", for: .normal)
        viewTitle.isHidden = true

        lblTitle.text = ""
        lblPrice.text = "- $120.00"
        btnCard.setTitleColor(appColor.black , for: .normal)
        
        btnBank.setTitleColor(appColor.theamGrayDark , for: .normal)

        selectCartType = true
        btnCard.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        btnBank.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
    }
}

extension WalletPaymentVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

