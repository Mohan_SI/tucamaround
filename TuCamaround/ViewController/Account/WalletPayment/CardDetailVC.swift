//
//  CardDetailVC.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class CardDetailVC: UIViewController {

    @IBOutlet weak var tfCardNum: UITextField!
    @IBOutlet weak var tfExp: UITextField!
    @IBOutlet weak var tfCVV: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfCardHName: UITextField!

    static func viewController () -> CardDetailVC {
        return "Home".viewController("CardDetailVC") as! CardDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfCardNum.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfExp.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfCVV.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfPrice.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfCardHName.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
  
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CardDetailVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int

        if textField == tfCardNum {
            //let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: "0123456789").invertedSet)
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)

            let components = newString.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted)
            //let decimalString = components.joinWithSeparator("") as NSString
            
            let decimalString = components.joined(separator: "") as NSString
            
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)

            if length == 0 || (length > 16 && !hasLeadingOne) || length > 19 {
                return (newLength > 16) ? false : true
            }
            
            var index = 0 as Int
            let formattedString = NSMutableString()

            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            
            if length - index > 4 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }

            if length - index > 4 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }
            
            if length - index > 4 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        } else if textField == tfCVV {
            return (newLength > 3) ? false : true
            
        } else if textField == tfExp {
            return (newLength > 4) ? false : true
        }
        
        return true
    }
}
