//
//  BankTransVC.swift
//  TuCamaround
//
//  Created by mac on 25/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class BankTransVC: UIViewController {
    
    static func viewController () -> BankTransVC {
        return "Home".viewController("BankTransVC") as! BankTransVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setVIewBorder()
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var view4DotShow: UIView!

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
 
    func setVIewBorder(){
   
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor =    #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1)
        yourViewBorder.lineDashPattern = [8, 8]
        yourViewBorder.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 48, height: 240) // view4DotShow.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.lineWidth = 1
        yourViewBorder.cornerRadius = 8
        yourViewBorder.path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 48, height: 240)).cgPath
        view4DotShow.layer.addSublayer(yourViewBorder)
 
    }
}
 
