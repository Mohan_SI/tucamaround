//
//  NotificationVC.swift
//  TuCamaround
//
//  Created by mac on 24/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    @IBOutlet weak var tfmName: UITextField!
    @IBOutlet weak var tfmDropD: UITextField!

    static func viewController () -> NotificationVC {
        return "Home".viewController("NotificationVC") as! NotificationVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfmName.superview?.border(.hexColor(0xE0E2E6), 8, 1)
        tfmDropD.superview?.border(.hexColor(0xE0E2E6), 8, 1)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NotificationVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
