//
//  Profile.swift
//  TuCamaround
//
//  Created by mac on 14/04/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol DetailsPhotoDelegate {
    func actionImgPreview(_ index: IndexPath)
}

class DetailsPhotoCell: UICollectionViewCell {
    @IBOutlet var imgVw: UIImageView!
    
    var delegate:DetailsPhotoDelegate!
    var index:IndexPath!
    
    @IBAction func actionImgPreview(_ sender: Any) {
        delegate.actionImgPreview(index)
    }
}

class ProfileReviewCell: UITableViewCell {
    @IBOutlet var imgProfile: ImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var viewRating: FloatRatingView!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblReview: UILabel!
}

class Profile: UIViewController {

    static func viewController () -> Profile {
        return "Settings".viewController("Profile") as! Profile
    }
    
    @IBOutlet var imgProfile: ImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var viewRating: FloatRatingView!
    @IBOutlet var lblRatingCount: UILabel!
    @IBOutlet var lblCompleted: UILabel!
    @IBOutlet var tblReview: UITableView!
    @IBOutlet var lblAbout: ExpandableLabel!
    @IBOutlet var cvPhotos: UICollectionView!
    @IBOutlet var consHtTbl: NSLayoutConstraint!
    @IBOutlet var lblReviewTitle: UILabel!
    @IBOutlet var viewPhotos: UIView!
    @IBOutlet var consHtVwPhotos: NSLayoutConstraint!
    
    var maImg = NSMutableArray()
    var dataReview = [JobReviewData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        wsReviewList()
        
        setData()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.pop(self)
    }

    //MARK: FUNCTIONS
    func setData() {
        let ob = LoginJSON.userInfo()
        lblName.text = ob?.data?.firstName.capFirstLetter()

        lblAbout.collapsedAttributedLink = NSAttributedString(string: "Leer más", attributes: [.foregroundColor:#colorLiteral(red: 0.9960784314, green: 0.2352941176, blue: 0, alpha: 1)])
        lblAbout.shouldCollapse = true
        lblAbout.textReplacementType = .character
        lblAbout.numberOfLines = 3
        lblAbout.text = ob?.data?.aboutus
        
        lblCompleted.text = "\(ob?.data?.performance ?? "")% Completados"
        
        if let totalReview = ob?.data?.totalReviews {
            lblRatingCount.attributedText = getAtr(txt: "\(totalReview) Calificaciones", clr: UIColor.white)
        }
        
        if let rating = ob?.data?.rating {
            if rating != "" {
                viewRating.rating = Double(rating)!
            }
        }
        
        lblRating.text = ob?.data?.rating

        if let img = ob?.data?.profileImage {
            imgProfile.sd_setImage(with: URL(string: img.profilePic((ob?.data?.id)!)), placeholderImage: UIImage(named: "def"))
        }
        
        if kAppDelegate.GetImage().count != 0 {
            maImg = kAppDelegate.GetImage()
            cvPhotos.reloadData()
        } else {
            consHtVwPhotos.constant = 0
            viewPhotos.isHidden = true
        }
    }
    
    func getAtr(txt: String, clr:UIColor) -> NSMutableAttributedString {
        let fontStyle = UIFont(name: "Rubik-Regular", size: 14.0)!
        
        let atr = NSMutableAttributedString(string: txt, attributes: [NSAttributedString.Key.foregroundColor: clr,NSAttributedString.Key.font : fontStyle, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        let final = NSMutableAttributedString()
        final.append(atr)
        
        return final
    }
    
    //MARK: wsReviewList
    func wsReviewList () {
        self.view.endEditing(true)

        let param = params()
        
        let httpPara = HttpParams (api_reviews_list)
        httpPara.params = param
        httpPara.popup = true
        httpPara.aai = true

        Http.instance().request(httpPara) { (response) in
            let json = response?.json as? NSDictionary
            json?.sessionExpired(self)
            
            //json?.msg(.fail)
            
            if json != nil {
                if json!.success() {
                    if let data = json?.data().data {
                        do {
                            let jsonn = try JSONDecoder().decode(MyReviewJSON.self, from: data)
                            if let data = jsonn.jobReviews {
                                if data.count == 0 {
                                    self.tblReview.isHidden = true
                                    self.lblReviewTitle.isHidden = true
                                } else {
                                    self.tblReview.isHidden = false
                                    self.lblReviewTitle.isHidden = false
                                    self.dataReview = data
                                    self.tblReview.reloadData()
                                }
                                
                                DispatchQueue.main.async {
                                    self.consHtTbl.constant = self.tblReview.contentSize.height
                                }
                            }
                        } catch let error {
                            print("Error : \(error)")
                        }
                    }
                } else {
                    self.tblReview.isHidden = true
                    self.lblReviewTitle.isHidden = true
                    //Http.alert("", string(json!, "message"))
                }
            }
        }
    }
}

extension Profile: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileReviewCell", for: indexPath) as! ProfileReviewCell
        
        let ob = dataReview[indexPath.row]
        
        cell.lblName.text = ob.firstName?.capFirstLetter()
        cell.lblReview.text = ob.comment
        cell.lblRating.text = ob.average
        
        if let amount = ob.bookingAmount {
            if amount != "" {
                cell.lblAmount.text = " \(Int(amount)!.withCommas()) USD -  \(ob.createdOn?.getTDate()?.timeAgoSinceDate() ?? "")"
            }
        }
        
        if let rating = ob.average {
            if rating != "" {
                cell.viewRating.rating = Double(rating)!
            }
        }
        
        if let img = ob.profileImage {
            cell.imgProfile.sd_setImage(with: URL(string: img.profilePic((ob.userId)!)), placeholderImage: UIImage(named: "def"))
        }
        
        return cell
    }
}

extension Profile: UICollectionViewDelegate, UICollectionViewDataSource, DetailsPhotoDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return maImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailsPhotoCell", for: indexPath) as! DetailsPhotoCell
        
        let obLogin = LoginJSON.userInfo()

        let dict = maImg.object(at: indexPath.row) as! NSDictionary
        
        cell.imgVw.layer.cornerRadius = 6.0
        
        cell.imgVw.sd_setImage(with: URL(string: string(dict, "image").workDone((obLogin?.data?.id)!)), placeholderImage: UIImage(named: ""))
        
        cell.delegate = self
        cell.index = indexPath
        
        return cell
    }
    
    func actionImgPreview(_ index: IndexPath) {
//        let obLogin = LoginJSON.userInfo()
//
//        let dict = maImg.object(at: index.row) as! NSDictionary
//
//        let previewImage = Bundle.main.loadNibNamed("PreviewImage", owner:
//                self, options: nil)?.first as? PreviewImage
//        kAppDelegate.window?.addSubview(previewImage!)
//            
//        previewImage?.imgVw.enableZoom()
//        previewImage?.imgVw.sd_setImage(with: URL(string: string(dict, "image").workDone((obLogin?.data?.id)!)), placeholderImage: UIImage(named: ""))
//        
//        previewImage?.frame = CGRect(x:0, y: 0, width: (kAppDelegate.window?.bounds.width)!, height: (kAppDelegate.window?.bounds.height)!)
    }
}

struct MyReviewJSON: Codable {
    let status: String?
    let jobReviews  : [JobReviewData]?
    let message : String?

    enum CodingKeys: String, CodingKey {
        case status, message
        case jobReviews = "job_reviews"
    }
}

class JobReviewData: Codable {
    
    let average: String?
    let bookingAmount: String?
    let comment: String?
    let createdOn: String?
    let firstName: String?
    let profileImage: String?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case average, comment
        case bookingAmount = "booking_amount"
        case createdOn = "created_on"
        case firstName = "first_name"
        case profileImage = "profile_image"
        case userId = "user_id"
    }
}
