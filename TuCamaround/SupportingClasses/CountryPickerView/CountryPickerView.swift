//
//  CountryPickerView.swift
//  Borobear
//
//  Created by mac on 03/10/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CountryPickerView: UIView , UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var viewEffect: UIVisualEffectView!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var pkrView: UIPickerView!
    var countryCodes = NSArray()

    var strType = ""
    // variable : -
    var onResult : ((NSDictionary)-> Void)!
    var arrList = NSArray()
    var isDateType = 0 // 0 date 1 time 2 both
    
    static let instance = CountryPickerView.initLoader()
    
    class func initLoader() -> CountryPickerView {
        return UINib(nibName: "CountryPickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CountryPickerView
    }
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        uiView.layer.cornerRadius = 2
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.addGestureRecognizer(gesture)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        remove()
    }
    
    
    
    
    func present(onCompletion: @escaping (NSDictionary)-> Void){
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)            //-->> add to subview
   //     self.arrList = arrList
      //  self.strType = title
        self.onResult = onCompletion
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

        countryCodes = kAppDelegate.countryCodesArr()
        self.pkrView.reloadAllComponents()

        displayUIview()
    }


    
    func displayUIview() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = 0
            self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.viewEffect.alpha = 0.5

            self.layoutIfNeeded()
        }) { (true) in
        }
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        remove()
    }
    
    @IBAction func actionOk(_ sender: UIButton) {
       let row = pkrView.selectedRow(inComponent: 0)
        let dict = countryCodes.object(at:row) as? NSDictionary
        self.onResult(dict!)
        remove()
    }
    
    func remove(){

        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = UIScreen.main.bounds.size.height
          //  self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.viewEffect.alpha = 0

        }) { (true) in
            self.removeFromSuperview()
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodes.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      //  let dict = arrList.object(at:row ) as? NSDictionary
        return ""
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width, height: 60))
        
        let myImageView = UIImageView(frame: CGRect(x: 50, y: 20, width: 20, height: 20))
        myImageView.contentMode = .scaleAspectFit
        var rowString = String()
        if let country = countryCodes[row] as? NSDictionary {
            
            rowString = "\(country.string("nicename")) (\(country.string("phonecode")))"
            myImageView.image =  UIImage(named: "\(country.string("iso").lowercased()).png")
            let myLabel = UILabel(frame: CGRect(x: 100, y: 10, width: pickerView.bounds.width - 100, height: 40))
            myLabel.font = UIFont.systemFont(ofSize: 16)//.init(name: "OpenSans", size: 14)
            myLabel.text = rowString
            myLabel.minimumScaleFactor = 0.5
            myLabel.adjustsFontSizeToFitWidth = true

            myView.addSubview(myLabel)
            myView.addSubview(myImageView)
            
        }

        return myView
    }

}
