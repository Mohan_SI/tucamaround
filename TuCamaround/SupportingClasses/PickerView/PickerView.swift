//
//  PickerView.swift
//  DsirRestaurant
//
//  Created by mac on 08/11/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class PickerView: UIView , UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var viewEffect: UIVisualEffectView!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var pkrView: UIPickerView!
    var onResult : ((NSDictionary)-> Void)!
    var arrList = NSArray()
    var isDateType = 0
    var strTitle = ""
    static let instance = PickerView.initLoader()
    
    class func initLoader() -> PickerView {
        return UINib(nibName: "PickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PickerView
    }
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        uiView.layer.cornerRadius = 2
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.addGestureRecognizer(gesture)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        remove()
    }
    func present(title:String , _ list:NSArray ,  onCompletion: @escaping (NSDictionary)-> Void){
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)
        self.onResult = onCompletion
        self.strTitle = title
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        arrList = list
         self.pkrView.reloadAllComponents()
        displayUIview()
    }
    
    func displayUIview() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = 0
            self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.viewEffect.alpha = 0.5
            
            self.layoutIfNeeded()
        }) { (true) in
        }
    }
    @IBAction func actionRemoveView(_ sender: UIButton) {
        remove()
    }
    @IBAction func actionYES(_ sender: UIButton) {
        if arrList.count > 0 {
            let row = pkrView.selectedRow(inComponent: 0)
            let dict = arrList.object(at:row) as? NSDictionary
            self.onResult(dict!)
        }
        remove()
    }
    func remove(){
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = UIScreen.main.bounds.size.height
            //  self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.viewEffect.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return arrList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
       let dict = arrList.object(at:row ) as? NSDictionary

        return dict?.string(self.strTitle)
    }
    
    
    
    
}

