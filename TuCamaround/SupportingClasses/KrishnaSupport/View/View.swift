//
//  View.swift
//  HarishFrameworkSwift4
//
//  Created by Harish on 11/01/18.
//  Copyright © 2018 Harish. All rights reserved.
//

import UIKit

open class View: UIView, LayoutParameters {
    var classPara: ClassPara = ClassPara()
    @IBInspectable open var isBorder: Bool = false
    @IBInspectable open var border: Int = 0
    @IBInspectable open var radious: Int = 0
    @IBInspectable open var borderColor: UIColor?
    @IBInspectable open var isShadow: Bool = false
    @IBInspectable open var shadowCColor: UIColor?
    @IBInspectable open var lsOpacity: CGFloat = 0.5
    @IBInspectable open var lsRadius: Int = 0
    @IBInspectable open var lsOffWidth: CGFloat = 2.0
    @IBInspectable open var lsOffHeight: CGFloat = 2.0
    @IBInspectable open var isStrokeColor: Bool = false
    override open func draw(_ rect: CGRect) {
        if isStrokeColor {
            strokeColor()
        }
    }
    var shadowLayer: CAShapeLayer!
    override open func layoutSubviews() {
        super.layoutSubviews()
        shadowCColor = UIColor.darkGray
        let obb = ClassPara ()
        obb.shadowLayer = shadowLayer
        obb.backgroundColor = backgroundColor
        obb.layer = layer
        classPara = obb
        layoutSubviews (self)
    }
}
public extension UIView {
    func border (_ color: UIColor?, _ cornerRadius: CGFloat, _ borderWidth: CGFloat) {
        self.layer.masksToBounds = true
        if color != nil {
            self.layer.borderColor = color?.cgColor
        }
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
    }
    func shadowSubViews () {
        self.backgroundColor = UIColor.clear
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
        let borderView = UIView()
        borderView.frame = self.bounds
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor = UIColor.black.cgColor
        borderView.layer.borderWidth = 1.0
        borderView.layer.masksToBounds = true
        self.addSubview(borderView)
        let otherSubContent = UIImageView()
        otherSubContent.frame = borderView.bounds
        borderView.addSubview(otherSubContent)
    }
    func shadow () {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
    }
    func shadow (_ color: UIColor) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
    }
    func shadow (_ radious: CGFloat, _ hoff: CGFloat) {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: hoff)
        //self.layer.
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = radious
        self.layer.masksToBounds = false
    }
    
    func clearBackground () {
        self.backgroundColor = UIColor.clear
    }
}

extension UIView {
    func x () -> CGFloat {
        return self.frame.origin.x
    }
    
    func y () -> CGFloat {
        return self.frame.origin.y
    }
    
    func width () -> CGFloat {
        return self.frame.size.width
    }
    
    func height () -> CGFloat {
        return self.frame.size.height
    }
}

class ShadowView: UIView {
    override open func draw(_ rect: CGRect) {
        if self.tag != 1 {
            self.tag = 315
        }
        
        self.backgroundColor = .clear
        
        self.dropShadow(radious: 8, extra1: self.tag)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(radious: CGFloat, extra1: Int = 0, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = radious
        
        var extra: CGFloat = 5
        
        var frame = bounds
        
        if extra1 > 300 && extra1 < 400 {
            let opacity = extra1 - 300
            
            extra = 5
            
            let shadowOpacity = Float(opacity) / 100
            
            layer.shadowOpacity = shadowOpacity
            
            frame.origin.x = frame.origin.x - extra
            frame.origin.y = frame.origin.y - extra
            frame.size.width = frame.size.width + (2 * extra)
            frame.size.height = frame.size.height + (2 * extra)
        } else if extra1 == 0 {
            extra = 5
            
            layer.shadowOpacity = 0.1
            
            frame.origin.x = frame.origin.x - extra
            frame.origin.y = frame.origin.y - extra
            frame.size.width = frame.size.width + (2 * extra)
            frame.size.height = frame.size.height + (2 * extra)
        } else {
            extra = 0
            
            layer.shadowOpacity = 0.02
            
            frame.origin.x = frame.origin.x - extra
            frame.origin.y = frame.origin.y - extra
            frame.size.width = frame.size.width + (2 * extra)
            frame.size.height = frame.size.height + (2 * extra)
        }
        
        layer.shadowPath = UIBezierPath(rect: frame).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIView {
    func dashedBorder (_ clr: UIColor) {
        let color = clr.cgColor
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        borderLayer.bounds=shapeRect
        borderLayer.position = CGPoint( x: frameSize.width/2,y: frameSize.height/2)
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.cornerRadius = 8
        borderLayer.lineWidth=1
        borderLayer.lineJoin=CAShapeLayerLineJoin.round
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        
        borderLayer.path = path.cgPath
        
        self.layer.addSublayer(borderLayer)
    }
}
