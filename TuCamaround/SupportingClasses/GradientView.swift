//
//  GradientView.swift
//  AMCoaching
//
//  Created by mac on 18/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

@IBDesignable class GradientViewGreen: UIImageView {
//        @IBInspectable open var firstColor: UIColor = #colorLiteral(red: 0.6588235294, green: 0.7764705882, blue: 0.2196078431, alpha: 1)
//        @IBInspectable open var secondColor: UIColor = #colorLiteral(red: 0.3607843137, green: 0.6823529412, blue: 0.2745098039, alpha: 1)
    //    @IBInspectable open var gradientType: CGFloat = 0
    
    //var firstColor: UIColor =  UIColor(red: 0.6588235294, green: 0.7764705882, blue: 0.2196078431, alpha: 1)
    //var secondColor: UIColor = UIColor(red: 0.3607843137, green: 0.6823529412, blue: 0.2745098039, alpha: 1)
    
    
    var firstColor: UIColor = #colorLiteral(red: 0.3490196078, green: 0.7019607843, blue: 0.4980392157, alpha: 1) // UIColor.hexColor(0x82CF00)//(red: 0.6588235294, green: 0.7764705882, blue: 0.2196078431, alpha: 1)
    var secondColor: UIColor = #colorLiteral(red: 0.431372549, green: 0.7764705882, blue: 0, alpha: 1)  //  UIColor.hexColor(0x72BC83)//(red: 0.3607843137, green: 0.6823529412, blue: 0.2745098039, alpha: 1)
    
    var vertical: Bool = false

    
    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [secondColor.cgColor, firstColor.cgColor]
        layer.startPoint = CGPoint.zero
        layer.cornerRadius = 8
        return layer
    }()
    
    //MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        applyGradient()
    }
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyGradient()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }
    
    //MARK: -
    
    func applyGradient() {
        self.backgroundColor = .clear
        updateGradientFrame()
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }
    
    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }
    
    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
    }
    
}






