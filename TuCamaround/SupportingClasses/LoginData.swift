//
//  UserInfo.swift
//  PacificPalmsProperty
//
//  Created by mac on 13/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

 
struct LoginData: Codable {
    static func logout() {
        let user = UserDefaults.standard
        user.removeObject(forKey: "UserData")
        user.removeObject(forKey: "userDetail")
        
        user.synchronize()
    }
    
    static  func saveUserInfo(data:NSDictionary) {
        if data.value(forKey: "id") != nil {
            
        }
        UserDefaults.standard.removeObject(forKey: "userDetail")
        let data = NSKeyedArchiver.archivedData(withRootObject: data)
        UserDefaults.standard.set(data, forKey: "userDetail")
    }
    static  func detail()->NSDictionary {
        if let data = UserDefaults.standard.object(forKey: "userDetail") as? Data {
            let userDict = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            return userDict
        }
        return NSDictionary()
    }
    
 
    
    static  var profile_pic : String {
        return detail().string("profile_pic")
    }

    static  var email : String {
        return detail().string("email")
    }
    
    static  var id : String {
        return detail().string("id")
    }
    
 
    
    static  var full_name : String {
        return detail().string("full_name")
    }
    

    static  var address : String {
        return detail().string("address")
    }

    
    
    
}
