//
//  Constants.swift
//  GogoEat
//
//  Created by Apple on 11/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

let ACCESSTOKENKEY = "access_token"

let DEVELOPERLOGS = false

//let server = "http://78.46.210.25:3000/"
let server = "http://78.46.210.25:3004/"

let serverseconds = ":00"

let BaseUrl                                                = "\(server)api/"

let api_generate_access_token                              = "\(BaseUrl)generate_access_token"
let api_user_registration                                  = "\(BaseUrl)user_registration"
let api_verify_account                                     = "\(BaseUrl)verify_account"
let api_resend_otp                                         = "\(BaseUrl)resend_otp"
let api_login                                              = "\(BaseUrl)login"
let api_forgot_password                                    = "\(BaseUrl)forgot_password"
let api_get_country                                        = "\(BaseUrl)get_country"
let api_get_my_profile                                     = "\(BaseUrl)get_my_profile"
let api_user_update_profile                                = "\(BaseUrl)user_update_profile"
let api_user_profilepic                                    = "\(BaseUrl)user_profilepic"
let api_change_password                                    = "\(BaseUrl)change_password"
let api_reset_password                                     = "\(BaseUrl)reset_password"
let api_delete_images                                      = "\(BaseUrl)delete_images"
let api_resend_mobile_otp                                  = "\(BaseUrl)resend_mobile_otp"
let api_verify_mobile                                      = "\(BaseUrl)verify_mobile"

let api_reviews_list                                       = "\(server)booking/my_reviews"
let api_post_list                                          = "\(server)booking/post_list"
let api_post_details                                       = "\(server)booking/post_details"
let api_post_bid                                           = "\(server)booking/post_bid"
let api_accept_declined_request                           = "\(server)booking/accept_declined_request"
let api_tech_give_review                                   = "\(server)booking/tech_give_review"
let api_tech_dashboard                                     = "\(server)booking/tech_dashboard"
let api_tech_address                                       = "\(server)booking/tech_address"
let api_admin_charges                                      = "\(server)booking/admin_charges"
let api_my_job_list                                        = "\(server)booking/my_job_list"
let api_help_support                                       = "\(server)booking/help_support"
let api_help_booking_list                                  = "\(server)booking/help_booking_list"
let api_help_support_input                                 = "\(server)booking/help_support_input"

extension String {
    func devicePic (_ id: String) -> String {
        return "\(server)uploads/devices/\(id)/\(self)"
    }
    
    func partnarPic() -> String {
        return "\(server)/uploads/partners/\(self)"
    }
    
    func categoryPic() -> String {
         return "\(server)/uploads/category/\(self)"
    }
    
    func jobReportPic (_ id: String) -> String {
        return "\(server)uploads/devices/\(id)/\(self)"
    }
    
    func speciPic () -> String {
        return "\(server)uploads/specialization/\(self)"
    }
    
    func servicePic () -> String {
        return "\(server)uploads/service/\(self)"
    }
    
    func profilePic (_ id: String) -> String {
        return "\(server)uploads/document/\(id)/\(self)"
    }
    
    func workDone (_ id: String) -> String {
        return "\(server)uploads/devices/\(id)/\(self)"
    }
    
    func catPic () -> String {
        return "\(server)uploads/category/\(self)"
    }
    
    func profilePicTech (_ id: String = "") -> String {
        if id.count == 0 {
            let ob = LoginJSON.userInfo()
            
            if ob?.data != nil {
                if ob?.data?.id != nil {
                    return "\(server)uploads/document/\((ob?.data?.id)!)/\(self)"
                }
            }
            
            return ""
        } else {
            return "\(server)uploads/document/\(id)/\(self)"
        }
    }

    var url: URL? {
        if let str = addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            return URL(string: str)
        }
        return nil
    }
    
    var urlPath: URL? {
        return URL(string: self)
    }
}

extension UIViewController {
    func statusBarColorChange(color:UIColor) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = color
            statusBar.tag = 100
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
}

private var rightViews = NSMapTable<UITextField, UIView>(keyOptions: NSPointerFunctions.Options.weakMemory, valueOptions: NSPointerFunctions.Options.strongMemory)
private var errorViews = NSMapTable<UITextField, UIView>(keyOptions: NSPointerFunctions.Options.weakMemory, valueOptions: NSPointerFunctions.Options.strongMemory)

extension UITextField {
    // Add/remove error message
    func setError(_ string: String? = nil, show: Bool = true) {
        if let rightView = rightView, rightView.tag != 999 {
            rightViews.setObject(rightView, forKey: self)
        }

        // Remove message
        guard string != nil else {
            if let rightView = rightViews.object(forKey: self) {
                self.rightView = rightView
                rightViews.removeObject(forKey: self)
            } else {
                self.rightView = nil
            }

            if let errorView = errorViews.object(forKey: self) {
                errorView.isHidden = true
                errorViews.removeObject(forKey: self)
            }

            return
        }

        // Create container
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false

        // Create triangle
        let triagle = TriangleTop()
        triagle.backgroundColor = .clear
        triagle.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(triagle)

        // Create red line
        let line = UIView()
        line.backgroundColor = .red
        line.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(line)

        // Create message
        let label = UILabel()
        label.text = string
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 15)
        label.backgroundColor = .black
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(label)

        // Set constraints for triangle
        triagle.heightAnchor.constraint(equalToConstant: 10).isActive = true
        triagle.widthAnchor.constraint(equalToConstant: 15).isActive = true
        triagle.topAnchor.constraint(equalTo: container.topAnchor, constant: -15).isActive = true
        triagle.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -15).isActive = true

        // Set constraints for line
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
        line.topAnchor.constraint(equalTo: triagle.bottomAnchor, constant: 0).isActive = true
        line.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 0).isActive = true
        line.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: 0).isActive = true

        // Set constraints for label
        label.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 0).isActive = true
        label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 0).isActive = true
        label.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: 0).isActive = true

        if !show {
            container.isHidden = true
        }
        // superview!.superview!.addSubview(container)
        //UIApplication.shared.keyWindow!.addSubview(container)
        self.superview?.superview?.addSubview(container)

        // Set constraints for container
        container.widthAnchor.constraint(lessThanOrEqualTo: superview!.widthAnchor, multiplier: 1).isActive = true
        container.trailingAnchor.constraint(equalTo: superview!.trailingAnchor, constant: 0).isActive = true
        container.topAnchor.constraint(equalTo: superview!.bottomAnchor, constant: 0).isActive = true

        // Hide other error messages
        let enumerator = errorViews.objectEnumerator()
        while let view = enumerator!.nextObject() as! UIView? {
            view.isHidden = true
        }

        if show {
            self.superview?.layer.borderColor = #colorLiteral(red: 0.8078431373, green: 0.08235294118, blue: 0, alpha: 1)
            
            // Add right button to textField
            let errorButton = UIButton(type: .custom)
            errorButton.tag = 999
            errorButton.setImage(UIImage(named: "error"), for: .normal)
            errorButton.frame = CGRect(x: 0, y: 0, width: frame.size.height, height: frame.size.height)
            errorButton.addTarget(self, action: #selector(errorAction), for: .touchUpInside)
            rightView = errorButton
            rightViewMode = .always
        } else {
            self.superview?.layer.borderColor = #colorLiteral(red: 0.8784313725, green: 0.8862745098, blue: 0.9019607843, alpha: 1)
            rightView = nil
            rightViewMode = .always
        }
        
        // Save view with error message
        errorViews.setObject(container, forKey: self)

    }

    // Show error message
    @IBAction
    func errorAction(_ sender: Any) {
//        let errorButton = sender as! UIButton
//        let textField = errorButton.superview as! UITextField
//
//        let errorView = errorViews.object(forKey: textField)
//        if let errorView = errorView {
//            errorView.isHidden.toggle()
//        }
//
//        let enumerator = errorViews.objectEnumerator()
//        while let view = enumerator!.nextObject() as! UIView? {
//            if view != errorView {
//                view.isHidden = true
//            }
//        }
        // Don't hide keyboard after click by icon
    }
}

class TriangleTop: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        context.beginPath()
        context.move(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.minX / 2.0), y: rect.maxY))
        context.closePath()

        context.setFillColor(#colorLiteral(red: 0.8078431373, green: 0.08235294118, blue: 0, alpha: 1).cgColor)
        context.fillPath()
    }
}

extension UIViewController {
    
    func changeImg(img:UIImageView, imgCheck:UIImage) {
        img.image = imgCheck
    }
    
    func changeTint(img:UIImageView, clr:UIColor) {
        img.image = img.image?.withRenderingMode(.alwaysTemplate)
        img.tintColor = clr
    }
    
    func alertWindow(_ str: String) {
        let alert = UIAlertController(title: "", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
