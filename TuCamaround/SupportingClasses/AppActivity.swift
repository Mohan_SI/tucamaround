//
//  AppActivity.swift
//  PacificPalmsProperty
//
//  Created by mac on 05/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
let KEY_ACCESSTOEKN = "access-token"

func generateAccessToken (completionHandler: @escaping (String?) -> Swift.Void) {
    let user = UserDefaults.standard
    
    if user.object(forKey: KEY_ACCESSTOEKN) == nil {
        
        let httpPara = HttpParams (api_generate_access_token)

        let md = NSMutableDictionary()
        md["device_id"] = UIDevice.current.identifierForVendor() //
        md["device_type"] = "2"
        md["api_key"] = "tocam@tech#2"

        httpPara.params = md
        httpPara.popup = false

        Http.instance().request(httpPara) { (response) in
            //let json = response?.json as? NSDictionary
            if let json = response?.json as? NSDictionary {
                if json.number("status").intValue == 1 {
                    let token = json.string("token")
                    
                    if token.count > 0 {
                        user.set(token, forKey: KEY_ACCESSTOEKN)
                        user.synchronize()
                        
                        completionHandler(token)
                        
                        return
                    }
                } else {
                    completionHandler (nil)
                }
            }
        }
        
        /*Http.instance().json(api_generate_access_token, params(), "POST", aai: false, popup: false, prnt: true, nil, nil, sync: false, defaultCalling: true) { (json, paras, str, data) in
            if let json = json as? NSDictionary {
                if json.number("status").intValue == 1 {
                    let token = json.string("token")
                    
                    if token.count > 0 {
                        user.set(token, forKey: KEY_ACCESSTOEKN)
                        user.synchronize()
                        
                        completionHandler(token)
                        
                        return
                    }
                } else {
                    completionHandler (nil)
                }
            }
            
            completionHandler (nil)
        }
        completionHandler (nil)*/
    } else {
        completionHandler (user.object(forKey: KEY_ACCESSTOEKN) as? String)
    }
}

func removeToken () {
    let user = UserDefaults.standard
    user.removeObject(forKey: KEY_ACCESSTOEKN)
    user.synchronize()
}

func params () -> NSMutableDictionary {
    let md = NSMutableDictionary()
    
    md["device_id"] = UIDevice.current.identifierForVendor() // UIDevice.current.identifierForVendor!.uuidString
    md["device_type"] = "2"
    md["api_key"] = "tocam@tech#2"
    md["access"] = "tech"
    
    let user = UserDefaults.standard
    
    if let token = user.object(forKey: KEY_ACCESSTOEKN) as? String {
        md["access_token"] = token
    }
    
    let ob = LoginJSON.userInfo()
    
    if ob != nil {
        if ob?.data?.id != nil {
            md["uid"] = (ob?.data?.id)!
        }
    }
    
    return md
}

func isLogged () -> Bool {
    let ob = LoginJSON.userInfo()
    
    if ob != nil {
        if ob?.data?.id != nil {
            return true
        }
    }
    
    return false
}

struct LoginJSON: Codable {
    static func logout () {
        let user = UserDefaults.standard
        user.removeObject(forKey: "UserData")
        user.synchronize()
    }
    
    static func userInfo () -> LoginJSON? {
        let user = UserDefaults.standard
        
        if let s1 = user.object(forKey: "UserData") as? String {
            let data = s1.data(using: String.Encoding.utf8)
            
            if data != nil {
                do {
                    let loginInfo = try JSONDecoder().decode(LoginJSON.self, from: data!)
                    return loginInfo
                } catch let error {
                    print("Error: \(error)")
                }
            }
        }
        
        return nil
    }
    
    let status: String
    let message: String
    let data: DataClass?
    //let images: [images]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
        //case images
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    let id: String
    let firstName, email, dateOfBirth: String
    let lastName: String?
    let gender, mobileNo, status: String
    let acVerifyStatus: String
    let password, access, nonverifyEmail, nonverifyMobile: String
    let nonverifyEmailCode, nonverifyMobileCode: String
    let country: String
    let countryCode: String
    let profileImage: String
    let address:String
    let age:String
    let rating: String
    let aboutus: String
    let totalReviews: String
    let performance: String
    
    enum CodingKeys: String, CodingKey {
        case id, country, address, age, rating, aboutus, performance
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case dateOfBirth = "date_of_birth"
        case gender
        case mobileNo = "mobile_no"
        case status
        case acVerifyStatus = "ac_verify_status"
        case password, access
        case nonverifyEmail = "nonverify_email"
        case nonverifyMobile = "nonverify_mobile"
        case nonverifyEmailCode = "nonverify_email_code"
        case nonverifyMobileCode = "nonverify_mobile_code"
        case countryCode = "country_code"
        case profileImage = "profile_image"
        case totalReviews = "total_reviews"
    }
}

// MARK: - DataClass
//struct images: Codable {
//    let id: String
//    let image: String
//    let jobId: String
//    let techId: String
//
//    enum CodingKeys: String, CodingKey {
//        case id, image
//        case jobId = "job_id"
//        case techId = "tech_id"
//    }
//}

extension UIViewController {
    func push (_ vc: UIViewController, _ bool: Bool = true) {
        vc.navigationController?.pushViewController(self, animated: bool)
    }
    
    func pop (_ vc: UIViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
}

extension NSDictionary {
    func sessionExpired (_ vc: UIViewController) {
        /*var boolSessionExpired = false
        
        if self.number("success").intValue == 4 {
            boolSessionExpired = true
        } else if self.string("response_message") == "invalid_access_token" {
            boolSessionExpired = true
        }
        
        if boolSessionExpired {
            kAppDelegate.subscribeToFirebase(false)
            
            LoginJSON.logout()
            
            let user = UserDefaults.standard
            user.removeObject(forKey: KEY_ACCESSTOEKN)
            user.synchronize()
            
            //Http.alert("", "Session expired")
            
            vc.popToHome (vc)
        }*/
    }
}

class AppActivity: NSObject {
    
    static  func setRemoveActivitys(){
        let user = UserDefaults.standard
        user.removeObject(forKey: "remamberMe")

        user.synchronize()
        
    }
    static var isUserLogin:Bool  {
        
        if LoginData.detail().count > 0 {
            return true
        }
        return false
    }
    
    static var isRemamber:Bool  {
        let user = UserDefaults.standard
        if let title = user.object(forKey: "remamberMe") {
            if title as? Int == 0 {
                return false
            }
        }
        return true
    }
    static  func setRemamberMe(_ isRemain:Bool){
        let user = UserDefaults.standard
        user.removeObject(forKey: "remamberMe")
        if isRemain {
            user.set(1, forKey: "remamberMe")
        }else {
            user.removeObject(forKey: "remamberMe")
            user.removeObject(forKey: "emailID")
            user.removeObject(forKey: "pass")
           // user.set(0, forKey: "remamberMe")
        }
        user.synchronize()
    }
    static func getCredentials() -> Credentials? {
        let user = UserDefaults.standard

        guard let id = user.value(forKey: "emailID") as? String else {
            return nil
        }
        guard let pass = user.value(forKey: "pass") as? String else {
            return nil
        }
       let ob = Credentials(emailID: id, pass: pass)
       return ob
    }
    
    static func setRemamberMe(_ email:String,_ pass:String){
        let user = UserDefaults.standard
   
        user.set(email, forKey: "emailID")
        user.set(pass, forKey: "pass")

        user.synchronize()
    }

    static func setSignInVC(){
        let user = UserDefaults.standard
        user.removeObject(forKey: KEY_ACCESSTOEKN)
        user.synchronize()
        //     AppActivity.setRemoveActivitys()
//        LoginData.logout()
        let vc = SignInVC.viewController()
        let nvc: UINavigationController = UINavigationController(rootViewController: vc)
        nvc.isNavigationBarHidden = true
        kAppDelegate.window?.rootViewController = nvc
    }


    

    
    static  func setShowSplash(_ yes:Bool){
        let user = UserDefaults.standard
       
        user.set(1, forKey: "isShowSplash")
        
        user.synchronize()
    }
    static var isShowSplash:Bool  {
        let user = UserDefaults.standard
        if let title = user.object(forKey: "isShowSplash") {
            if title as? Int == 1 {
                return true
            }
        }
        return false
    }
    
}
struct Credentials {
    let emailID :String
    let pass :String
}

class cCode: NSObject {
    var code :String = ""
    var icon:UIImage? = nil
}

extension UIView {
    
func dashedBorderLayerWithColor(_ color: CGColor) -> CAShapeLayer {
    
    let  borderLayer = CAShapeLayer()
    borderLayer.name  = "borderLayer"
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
    
    borderLayer.bounds=shapeRect
    borderLayer.position = CGPoint( x: frameSize.width/2,y: frameSize.height/2)
    borderLayer.fillColor = UIColor.clear.cgColor
    borderLayer.strokeColor = color
    borderLayer.cornerRadius = 8
    borderLayer.lineWidth=1
    borderLayer.lineJoin=CAShapeLayerLineJoin.round
    borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
    
    let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
    
    borderLayer.path = path.cgPath
    
    return borderLayer
    
}
}


extension UIView {
    func sdBorder (_ radious: CGFloat = 8) {
        self.border0(radious)
    }
}

extension UITextField {
    func sdTFBorder (_ radious: CGFloat = 8, _ color: UIColor = .hexColor(0xBCBFC6)) {
        //self.superview?.border(.hexColor(0xBCBFC6), radious, 1)
        
        self.superview?.border(color, radious, 1)
    }
}

class MyTapGesture: UITapGestureRecognizer {
    var lbl = UILabel()
}

//For Label selection perticuler text
extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
         let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

extension String {
    func maxLength(length: Int) -> String {
        var str = self
        let nsString = str as NSString
        if nsString.length >= length {
            str = nsString.substring(with:
                NSRange(
                    location: 0,
                    length: nsString.length > length ? length : nsString.length)
            )
        }
        return  str
    }
}

extension Date {
    
    func timeAgoSinceDate() -> String {
        
        // From Time
        let fromDate = self
        
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }
        
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }
        
        return "a moment ago"
    }
}

@IBDesignable
class ShadowVieww: UIView {
    //Shadow
    @IBInspectable var shadowColor: UIColor = UIColor.black {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 3, height: 3) {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 15.0 {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var cornarRadius: CGFloat = 0.5 {
        didSet {
            self.updateView()
        }
    }
  
    //Apply params
    func updateView() {
        self.layer.cornerRadius = self.cornarRadius
        self.layer.shadowColor = self.shadowColor.cgColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = self.shadowOffset
        self.layer.shadowRadius = self.shadowRadius
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "$"
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
