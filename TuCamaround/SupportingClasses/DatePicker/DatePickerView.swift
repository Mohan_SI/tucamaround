//
//  DatePickerView.swift
//  DsirRestaurant
//
//  Created by mac on 12/11/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class DatePickerView: UIView  {
    
    @IBOutlet weak var viewEffect: UIVisualEffectView!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var onResult : ((Date)-> Void)!
    var arrList = NSArray()

    var isDateType = 0
    var strTitle = ""
    static let instance = DatePickerView.initLoader()
    class func initLoader() -> DatePickerView {
        return UINib(nibName: "DatePickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DatePickerView
    }
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        uiView.layer.cornerRadius = 2
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.addGestureRecognizer(gesture)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        remove()
    }
    func present(minimumDate:Date? ,  onCompletion: @escaping (Date)-> Void){
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)
        self.onResult = onCompletion
       
        datePicker.datePickerMode = .date
        if minimumDate != nil {
            datePicker.minimumDate = minimumDate
            let tomorrow = Calendar.current.date(byAdding: .day, value: 15, to: minimumDate!)
            datePicker.maximumDate = tomorrow
        }
      

        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
 
        displayUIview()
    }
    
    func displayUIview() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = 0
            self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.viewEffect.alpha = 0.5
            
            self.layoutIfNeeded()
        }) { (true) in
        }
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        remove()
    }
    @IBAction func actionRemoveView(_ sender: UIButton) {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd/MM/yyyy"
//        let ssss = formatter.string(from: datePicker.date)
        self.onResult(datePicker.date)
        remove()
    }
    func remove(){
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = UIScreen.main.bounds.size.height
            //  self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.viewEffect.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    
}
