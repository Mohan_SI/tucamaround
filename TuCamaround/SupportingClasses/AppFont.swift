//
//  AppFont.swift
//  PacificPalmsProperty
//
//  Created by mac on 06/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class AppFont {
    static let Medium = "Rubik-Medium"
    static let Bold = "Rubik-Bold"
    static let Regular = "Rubik-Regular"
}
