//
//  UIColors.swift
//  PacificPalmsProperty
//
//  Created by mac on 05/03/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class appColor : NSObject {
    static let theamGrapeColor = #colorLiteral(red: 0.3333333333, green: 0.1254901961, blue: 0.5176470588, alpha: 1)
    static let theamRedColor = #colorLiteral(red: 1, green: 0.1529411765, blue: 0.1529411765, alpha: 1)
    static let theamOrangeColor = #colorLiteral(red: 0.9960784314, green: 0.2352941176, blue: 0, alpha: 1)
    static let theamGreenColor = #colorLiteral(red: 0.003921568627, green: 0.7529411765, blue: 0.537254902, alpha: 1)
    static let theamDarkGreenColor = #colorLiteral(red: 0.2509803922, green: 0.3450980392, blue: 0.2156862745, alpha: 1)
    
    
    static let theamGrayDark = #colorLiteral(red: 0.5137254902, green: 0.5411764706, blue: 0.6117647059, alpha: 1)
    static let theamLightGrayDark = #colorLiteral(red: 0.8784313725, green: 0.8862745098, blue: 0.9019607843, alpha: 1)
    static let border = #colorLiteral(red: 0.737254902, green: 0.7490196078, blue: 0.7764705882, alpha: 1)

    static let black = #colorLiteral(red: 0.07843137255, green: 0.1058823529, blue: 0.1843137255, alpha: 1)

    
    static let shimmerColor = #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1)
    static let error = #colorLiteral(red: 0.6823529412, green: 0, blue: 0, alpha: 1)
    static let grayLine = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
    static let editing = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.2980392157, alpha: 1)
    static let placeHolder = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
}

extension UIImageView {
 
    func applyAnimation() {
         let expandTransform:CGAffineTransform = CGAffineTransform(scaleX: 1.15, y: 1.15);
    
            UIView.transition(with: self,
                              duration:0.1,
                              options: UIView.AnimationOptions.transitionCrossDissolve,
                              animations: {
                                
                                self.transform = expandTransform
            },
                              completion: {(finished: Bool) in
                                UIView.animate(withDuration: 0.4,
                                               delay:0.0,
                                               usingSpringWithDamping:0.40,
                                               initialSpringVelocity:0.2,
                                               options:UIView.AnimationOptions.curveEaseOut,
                                               animations: {
                                                self.transform = expandTransform.inverted()
                                }, completion:nil)
            })
        
    }
}


