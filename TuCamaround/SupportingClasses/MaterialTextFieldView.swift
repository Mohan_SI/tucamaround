//
//  MaterialTextField.swift
//  TapATrady
//
//  Created by Apple on 19/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate

enum FieldStatus {
    case none
    case varified
    case notvarified
    case error
    case nodata
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
protocol MaterialTextFieldViewDelegate {
    func materialTextChanged (_ textField: UITextField)
}

class MaterialTextFieldView: UIView {
    
    override open func draw(_ rect: CGRect) {
        delegate = self.parentViewController as? UITextFieldDelegate
        
        if textField == nil {
            addSubViews ()
        }
    }
    
    var materialDelegate: MaterialTextFieldViewDelegate?
    
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
    
    var status: FieldStatus = .nodata
    
    var borderWidth = 2
    var minusWidth: CGFloat = 0
    var radious = 8
    
    var boolSearchKey = false
    
    var hideRight = true
    
    var placeHolderGap = 15
    var tfFontSize = 16
    var placeHolderfontSize = 16
    var msgFontSize = 12
    
    var tfFontFamily = AppFont.Regular // "JosefinSans-Regular"
    var placeHolderfontFamily = AppFont.Regular // "JosefinSans-Regular"
    var msgFontFamily = AppFont.Regular // "JosefinSans-Regular"
    
    var messageText = ""
    var placeHolder = ""
    var text = ""
    
    var textColor = UIColor.hexColor(0x141B2F)
    
    var textFieldBackgroundColor = UIColor.hexColor(0x707070)
    
    var placeHolderColor = UIColor.hexColor(0x696969)
    var borderColor = UIColor.hexColor(0xD9E5DE)
    var messageColor = UIColor.hexColor(0x707070)
    
    var messageErrorColor = appColor.error
    var borderErrorColor = appColor.error //UIColor.hexColor(0xFF2727)
    var placeHolderErrorColor = appColor.error //UIColor.hexColor(0xFF2727)
    
    var textField: UITextField!
    var lblMsg: UILabel!
    var delegate: UITextFieldDelegate?
    var lblPlace: UILabel!
    var lblPlaceLine: UILabel!
    var imgPassVisible: UIImageView!
    var imgLIcon: UIImage!
    var rightPadding = 0

    var isEnabled = true
    var isSecureTextEntry = false
    var _showpassword = false
    var imgIconView: UIImageView!

    var keyboardType: UIKeyboardType! = .default
    let PlaceY:CGFloat = 20
    var autocapitalizationType: UITextAutocapitalizationType? = .sentences
    var isLeftView = false
    var isMobileView = false
    func redraw () {
        for vv in self.subviews {
            vv.removeFromSuperview()
        }
        
        addSubViews ()
    }
    
    func addSubViews () {
        let height = self.frame.size.height
        let tfH:CGFloat = 56.0//3 * (height / 4)
        let placeH:CGFloat = 18//tfH / 2
        let extra: CGFloat = 14
        
        lblPlace = UILabel(frame: CGRect(x: CGFloat(placeHolderGap)-(extra/2), y: PlaceY, width: 0, height: placeH))
        lblPlace.text = placeHolder//placeHolder
        lblPlace.backgroundColor = UIColor.clear
        lblPlace.textAlignment = .center
        
        if placeHolderfontFamily.count > 0 {
            lblPlace.font = UIFont(name: placeHolderfontFamily, size: CGFloat(placeHolderfontSize))
        }
        
        lblPlace.numberOfLines = 0
        lblPlace.sizeToFit()
        lblPlace.frame.size.width = lblPlace.frame.size.width + extra
        lblPlaceLine = UILabel()
        lblPlaceLine.frame = lblPlace.frame
        lblPlaceLine.frame.origin.y = lblPlace.frame.origin.y + (lblPlace.frame.size.height/2)
        lblPlaceLine.frame.size.height = 2
        
        lblPlaceLine.backgroundColor = UIColor.white
        self.addSubview(lblPlaceLine)
        self.addSubview(lblPlace)
        
        textField = UITextField(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: tfH))

 
        if isSecureTextEntry {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width:
                50, height: 40))
            imgPassVisible = UIImageView(frame: CGRect(x: 10, y: 10, width: 24, height: 24) )
            textField.isSecureTextEntry = true
            imgPassVisible.image = #imageLiteral(resourceName: "eyeLine")
            paddingView.addSubview(imgPassVisible)
            let btb = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height:36 )  )
            paddingView.addSubview(btb)
            btb.addTarget(self, action: #selector(actionCall(btn:)), for: .touchUpInside)
            
            paddingView.backgroundColor = .clear
            textField.rightView = paddingView
            textField.rightViewMode = .always
           // textField.padding1(rightPadding) //paddingRight(rightPadding)

        } else {
            if rightPadding > 0 {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width:
                    rightPadding, height: 0))
                paddingView.backgroundColor = .clear
                textField.rightView = paddingView
                textField.rightViewMode = .always
                //textField.padding1(rightPadding)
            }
        }
        if isLeftView {
            if isMobileView {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: placeHolderGap, height: 40))
                paddingView.backgroundColor = .red
                imgIconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 24, height: 24) )
                textField.isSecureTextEntry = true
                imgIconView.image = imgLIcon
                paddingView.addSubview(imgIconView)
                paddingView.backgroundColor = .clear
                let lineView = UIView(frame: CGRect(x: 40, y: 4, width: 1, height: 30))
                lineView.backgroundColor = appColor.theamLightGrayDark
                paddingView.addSubview(lineView)
                let lblTitle = UILabel(frame: CGRect(x: 56, y: 6, width: 40, height: 30))
                lblTitle.text = "+507"
                
                lblTitle.font = UIFont(name: placeHolderfontFamily, size: CGFloat(placeHolderfontSize))
                
                lblTitle.textColor = appColor.black
                paddingView.addSubview(lblTitle)
                
                let lineView1 = UIView(frame: CGRect(x: 120, y: 4, width: 1, height: 30))
                lineView1.backgroundColor = appColor.theamLightGrayDark
                paddingView.addSubview(lineView1)
                
                textField.leftView = paddingView
                textField.leftViewMode = .always
                
            }else {
                
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: placeHolderGap, height: 40))
                paddingView.backgroundColor = .red
                imgIconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 24, height: 24) )
                textField.isSecureTextEntry = true
                imgIconView.image = imgLIcon
                paddingView.addSubview(imgIconView)
                let lineView = UIView(frame: CGRect(x: 40, y: 4, width: 1, height: 30))
                paddingView.backgroundColor = .clear
                lineView.backgroundColor = appColor.theamLightGrayDark
                paddingView.addSubview(lineView)
                textField.leftView = paddingView
                textField.leftViewMode = .always
            }
            

        }else {
            textField.paddingL(placeHolderGap)

        }
        
        

        textField.border(borderColor, CGFloat(radious), CGFloat(borderWidth))
        
        if tfFontFamily.count > 0 {
            textField.font = UIFont(name: tfFontFamily, size: CGFloat(tfFontSize))
        }
        
        if boolSearchKey {
            textField.returnKeyType = .search
        }
        
        textField.delegate = delegate
      //  textField.padding1(placeHolderGap)
        textField.keyboardType = keyboardType
        textField.isEnabled = isEnabled
         textField.backgroundColor = UIColor.white
        textField.isSecureTextEntry = isSecureTextEntry
        
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        
        if autocapitalizationType != nil {
            textField.autocapitalizationType = autocapitalizationType!
        } else {
            textField.autocapitalizationType = .none
        }
        
        textField.text =  text
        
        if text.count > 0 {
            self.endEditing()
        }
        self.addSubview(textField)
        self.addSubview(lblPlace)
        
        let msgH : CGFloat = 16.0
        let lblMsg = UILabel(frame: CGRect(x: CGFloat(placeHolderGap), y: tfH + 1, width: self.frame.size.width-CGFloat(placeHolderGap), height: msgH))
        
        //print("msgFontFamily-\(msgFontFamily)-\(msgFontSize)-\(CGFloat(msgFontSize))-")
        
        if msgFontFamily.count > 0 {
            let font = UIFont(name: msgFontFamily, size: CGFloat(msgFontSize))
            //print("font-\(font)-")
            lblMsg.font = font//UIFont(name: msgFontFamily, size: CGFloat(msgFontSize))
        }
        
        lblMsg.adjustsFontSizeToFitWidth = true
        lblMsg.minimumScaleFactor = 0.2
        lblMsg.text = messageText//messageText
        lblMsg.textColor = UIColor.red
        
        self.addSubview(lblMsg)
        self.lblMsg = lblMsg
        
        /*let iii_image = #imageLiteral(resourceName: "Group 4081")
        
        if iii_image != nil {
            imgValidation = UIImageView(frame: CGRect(x: textField.frame.size.width - 30, y: (textField.frame.size.height/2) - 10, width: 20, height: 20))
            imgValidation?.image = iii_image
            self.addSubview(imgValidation!)
         
            imgValidation?.isHidden = hideRight
        }*/
        
        btnClear = UIButton(frame: CGRect(x: textField.frame.size.width - 50, y: 0, width: 50, height: textField.frame.size.height))
        btnClear!.addTarget(self, action: #selector(self.actionClearData(_:)), for: .touchUpInside)
        self.addSubview(btnClear!)
        
        btnClear?.isHidden = true
        
        lblMsg.textColor = messageColor
        textField.layer.borderColor = borderColor.cgColor
        textField.textColor = textColor
        lblPlace.textColor = placeHolderColor
        
        if text.count > 0 {
            let place = lblPlace.frame
            self.addSubview(lblPlace)
            self.lblPlace.frame.origin.y = (place.size.height / 2) * (-1)
        } else {
            status = .nodata
        }
        
        if status == .varified {
            showNoError()
            lblMsg.text = ""
        }
        
        var count = 0
        
        for vv in self.subviews {
            count += 1
            
            if let vvv = vv as? UIImageView {
                if vvv.frame.size.width == 24 {
                    vvv.image = #imageLiteral(resourceName: "lv")
                    self.addSubview(vvv)
                    break
                }
            }
        }
        
        self.backgroundColor = UIColor.clear
        
        //textField.backgroundColor = UIColor.red
        
        self.addSubview(lblPlaceLine)
        self.addSubview(lblPlace)
        
        textField.addTarget(self, action: #selector(self.actionTfSearch5(_:)), for: .editingChanged)
        
        //kAppDelegate.findLangOutlet(self)
    }
    
    @objc func actionCall(btn:UIButton) {
        _showpassword = !_showpassword
        if _showpassword {
            textField.isSecureTextEntry = false
            imgPassVisible.image = #imageLiteral(resourceName: "eye")
        } else {
            textField.isSecureTextEntry = true
            imgPassVisible.image = #imageLiteral(resourceName: "eyeLine")
        }

    }
    
    @IBAction func actionTfSearch5(_ sender: UITextField) {
        //materialDelegate?.materialTextChanged(sender)
    }
    
    @objc func actionClearData (_ sender: UIButton) {
        textField.text = ""
        
        textField.resignFirstResponder()
        
        endEditing ()
        
        showNoValidation ()
    }
    
    func setNormal () {
        endEditing ()
        lblMsg.textColor = .clear
        textField.layer.borderColor = borderErrorColor.cgColor
        lblPlace.textColor = placeHolderErrorColor

    }
    
    var btnClear: UIButton?
    var imgValidation: UIImageView?
    
    func showError () {
        imgValidation?.image = #imageLiteral(resourceName: "Group 40861")
        btnClear?.isHidden = false
        
        lblMsg.textColor = messageErrorColor
        textField.layer.borderColor = borderErrorColor.cgColor
        lblPlace.textColor = placeHolderErrorColor
        
        lblMsg.isHidden = false
    }
    
    func showNoError () {
        imgValidation?.image = #imageLiteral(resourceName: "Group 4086")
        btnClear?.isHidden = true
        
        lblMsg.textColor = messageColor
        textField.layer.borderColor = borderColor.cgColor
        lblPlace.textColor = placeHolderColor
        
        lblMsg.isHidden = true
    }
    
    func showNoValidation () {
        imgValidation?.image = #imageLiteral(resourceName: "Group 4081")
        btnClear?.isHidden = true
        
        lblMsg.textColor = messageColor
        textField.layer.borderColor = borderColor.cgColor
        lblPlace.textColor = placeHolderColor
        
        lblMsg.isHidden = true
    }
    
    func setData (_ key: String, _ extra: String = "") {
        if let value = key.getUserValue() as? String {
            setText(value)
        }
    }
    
    func setText (_ text: String) {
        if textField != nil {
            textField.text = text
            self.text = kAppDelegate.getLang(text)
            
            if text.count > 0 {
                self.status = .varified
                beginEditing()
                showNoError()
                lblMsg.text = ""
            } else {
                endEditing() 
            }
        }
    }
    
    func beginEditing () {
        let place = lblPlace.frame
        self.addSubview(lblPlaceLine)
        self.addSubview(lblPlace)
        
        let color = appColor.theamOrangeColor
        textField.layer.borderColor = color.cgColor
        lblPlace.textColor = color
        lblMsg.text = ""
        
        UIView.animate(withDuration: 0.1) {
            self.lblPlace.frame.origin.y = (place.size.height / 2) * (-1)
            //self.lblPlaceLine.frame.origin.y = self.lblPlace.frame.origin.y
            self.lblPlaceLine.frame.origin.y = self.lblPlace.frame.origin.y + (self.lblPlace.frame.size.height/2)
        }
    }

    func endEditing () {
        if textField.text?.count == 0 {
            self.addSubview((self.lblPlaceLine)!)
            self.addSubview((self.lblPlace)!)
            
            UIView.animate(withDuration: 0.1) {
                self.lblPlace.frame.origin.y = self.PlaceY
                //self.lblPlaceLine.frame.origin.y = 18
                self.lblPlaceLine.frame.origin.y = self.lblPlace.frame.origin.y + (self.lblPlace.frame.size.height/2)
            }
        }
    }
}

let MAX_LENGTH_PHONENUMBER = 15
let ACCEPTABLE_MOBILE_NUMBERS     = "0123456789"
let ACCEPTABLE_NUMBERS     = "0123456789.,"
let ACCEPTABLE_NUMBERSCamma     = "0123456789,"

//MARK:- Validation
extension MaterialTextFieldView {
    func showValidation (_ boolError: Bool, _ msg: String, _ status: FieldStatus) {
        if boolError {
            (textField.superview as? MaterialTextFieldView)?.status = status
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = msg
            (textField.superview as? MaterialTextFieldView)?.showError()
        } else {
            (textField.superview as? MaterialTextFieldView)?.status = status
            (textField.superview as? MaterialTextFieldView)?.showNoError()
        }
    }
    
    
    func checkEmpty (_ empty: String) -> Bool {
        
        if textField.text!.count == 0 {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.showError()
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = empty
            (textField.superview as? MaterialTextFieldView)?.lblMsg.isHidden = false
            
            return false
        } else if textField.text!.count > 0 {
            (textField.superview as? MaterialTextFieldView)?.status = .varified
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = ""
            (textField.superview as? MaterialTextFieldView)?.showNoError()
            (textField.superview as? MaterialTextFieldView)?.lblMsg.isHidden = false
        }
        
        return true
    }
    

    
    func email (_ empty: String = messages.email_empty, _ wrong: String = messages.email_empty) -> Bool {
        if textField.text!.count == 0 {
            
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = empty
            (textField.superview as? MaterialTextFieldView)?.showError()
            
            return false
            
        } else {
            let bool = validEmail (textField.text!)
            
            if bool {
                (textField.superview as? MaterialTextFieldView)?.status = .varified
                (textField.superview as? MaterialTextFieldView)?.showNoError()
                (textField.superview as? MaterialTextFieldView)?.lblMsg.text = ""
                
                return true
            } else {
                (textField.superview as? MaterialTextFieldView)?.status = .error
                (textField.superview as? MaterialTextFieldView)?.lblMsg.text = messages.valid_email
                (textField.superview as? MaterialTextFieldView)?.showError()
                
                return false
            }
        }
    }
    
    func password (_ empty: String = messages.password_empty, _ minimum: String = messages.password_valid) -> Bool {
        if textField.text!.count == 0 {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = empty
            (textField.superview as? MaterialTextFieldView)?.showError()
            return false
        }else if textField.text!.count < 8 {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = minimum
            (textField.superview as? MaterialTextFieldView)?.showError()
            return false
        } else {
            let bool = validPassword (self.textField.text!)
            
            if bool {
                (textField.superview as? MaterialTextFieldView)?.status = .varified
                (textField.superview as? MaterialTextFieldView)?.showNoError()
                
                return true
            } else {
                (textField.superview as? MaterialTextFieldView)?.status = .error
                (textField.superview as? MaterialTextFieldView)?.lblMsg.text = messages.password_valid
                (textField.superview as? MaterialTextFieldView)?.showError()
                
                return false
            }
        }
    }
    func confirmPassword ( confirmPass:String ,   empty: String , minimum: String , password_not_match:String) -> Bool {
     
        
        if textField.text!.count == 0 {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = empty
            (textField.superview as? MaterialTextFieldView)?.showError()
            return false
        }/*else if textField.text!.count < 8 {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = minimum
            (textField.superview as? MaterialTextFieldView)?.showError()
            return false
        }*/else if confirmPass != self.textField.text! {
            (textField.superview as? MaterialTextFieldView)?.status = .error
            (textField.superview as? MaterialTextFieldView)?.lblMsg.text = password_not_match
            (textField.superview as? MaterialTextFieldView)?.showError()
            return false
        }
        else {
//            if confirmPass != self.textField.text! {
//                (textField.superview as? MaterialTextFieldView)?.status = .error
//                (textField.superview as? MaterialTextFieldView)?.lblMsg.text = password_not_match
//                (textField.superview as? MaterialTextFieldView)?.showError()
//
//                return false
//            }
            (textField.superview as? MaterialTextFieldView)?.status = .varified
            (textField.superview as? MaterialTextFieldView)?.showNoError()
            
            return true
          
        }
    }
    
    

}



func validPassword(_ testStr: String) -> Bool {
    if testStr.count >= 8 {
        return true
    }
        return false
    let reg = "^(?=.*[0-9])(?=.*[A-Z])[A-Za-z\\d$@$#!%*?&]{8,}"
    let res = NSPredicate(format:"SELF MATCHES %@", reg)
    return res.evaluate(with: testStr)
}

func validEmail(_ testStr: String) -> Bool {
    let reg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let res = NSPredicate(format:"SELF MATCHES %@", reg)
    return res.evaluate(with: testStr)
}

let emptyEmailMobileMsg = "Please enter Email / Mobile no."
let wrongEmailMobileMsg = "Please enter valid Email / Mobile no."

let emptyEmailMsg = "Please enter Email."
let emptyMobileMsg = "Please enter Mobile no."

let wrongEmailMsg = "Please enter valid Email."
let wrongMobileMsg = "Mobile no. must have minimum 8 digits."

extension UILabel{
    public var requiredWidth: CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: frame.height))
        label.numberOfLines = 1
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.attributedText = attributedText
        label.sizeToFit()
        return label.frame.width
    }
}
extension UITextField {
/*    func paddingRight (_ pad: Int) {
        DispatchQueue.main.async {
            let paddingView = UIView(frame: CGRect(x: Int(self.frame.size.width-CGFloat(pad)),
                                                   y: 0,
                                                   width: pad,
                                                   height: Int(self.frame.size.height)))
            self.leftView = paddingView
            self.leftViewMode = UITextField.ViewMode.always
        }
    }*/
}
